# Black Spheres experiment simulation with usage of multibody
1. load Release_mechanism.prj
2. run black_spheres_experiments_in_zarm_model.slx (created in Matlab 2020a)

## TODO list:
1. create easy to find default values,
2. more variants -> platform, spatial forces
3. adapt motors trajectory from real experiments
4. get rid of magic numbers in the model
5. create live script
6. create app

enjoy the pic:)

![snap](./pics/snap.JPG)