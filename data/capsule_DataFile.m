% Simscape(TM) Multibody(TM) version: 7.1

% This is a model data file derived from a Simscape Multibody Import XML file using the smimport function.
% The data in this file sets the block parameter values in an imported Simscape Multibody model.
% For more information on this file, see the smimport function help page in the Simscape Multibody documentation.
% You can modify numerical values, but avoid any other changes to this file.
% Do not add code to this file. Do not edit the physical units shown in comments.

%%%VariableName:smiData


%============= RigidTransform =============%

%Initialize the RigidTransform structure array by filling in null values.
smiData.RigidTransform(151).translation = [0.0 0.0 0.0];
smiData.RigidTransform(151).angle = 0.0;
smiData.RigidTransform(151).axis = [0.0 0.0 0.0];
smiData.RigidTransform(151).ID = '';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(1).translation = [0 0 0];  % mm
smiData.RigidTransform(1).angle = 0;  % rad
smiData.RigidTransform(1).axis = [0 0 0];
smiData.RigidTransform(1).ID = 'AssemblyGround[AKKUPLATTFORM_43:AKKUPLATTFORM_ALU_1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(2).translation = [382.10894746105896 251.01750911840597 357.62535766369695];  % mm
smiData.RigidTransform(2).angle = 2.0251096903870613;  % rad
smiData.RigidTransform(2).axis = [0.30430026295084406 -0.94558455365724592 -0.1152007024842906];
smiData.RigidTransform(2).ID = 'AssemblyGround[AKKUPLATTFORM_43:AKKU_BAUGRUPPE_VAR_1_17:GRUNDPLATTE_GROSS_2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(3).translation = [299.50939871237898 131.46994988081201 251.13148165738397];  % mm
smiData.RigidTransform(3).angle = 2.0251096903870316;  % rad
smiData.RigidTransform(3).axis = [0.30430026295084489 -0.94558455365724992 -0.11520070248425598];
smiData.RigidTransform(3).ID = 'AssemblyGround[AKKUPLATTFORM_43:AKKU_BAUGRUPPE_VAR_1_17:KLEMMBÜGEL_3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(4).translation = [225.280172579213 339.52379723486206 352.46567425222895];  % mm
smiData.RigidTransform(4).angle = 2.9458566544388352;  % rad
smiData.RigidTransform(4).axis = [-0.80588976306733684 -0.25934483157777882 0.5322423772278615];
smiData.RigidTransform(4).ID = 'AssemblyGround[AKKUPLATTFORM_43:AKKU_BAUGRUPPE_VAR_1_17:KLEMMBÜGEL_4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(5).translation = [324.74906499004999 338.34274906405301 139.49432742877599];  % mm
smiData.RigidTransform(5).angle = 2.520752740591012;  % rad
smiData.RigidTransform(5).axis = [0.7872568340218119 -0.40392157237672932 -0.46591205248916218];
smiData.RigidTransform(5).ID = 'AssemblyGround[AKKUPLATTFORM_43:AKKU_BAUGRUPPE_VAR_1_17:KLEMMBÜGEL_5]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(6).translation = [200.04050630155194 132.65099805154802 464.10282848080095];  % mm
smiData.RigidTransform(6).angle = 2.2222714552332565;  % rad
smiData.RigidTransform(6).axis = [-0.42916196402074913 -0.83645121277553725 0.34083629103161239];
smiData.RigidTransform(6).ID = 'AssemblyGround[AKKUPLATTFORM_43:AKKU_BAUGRUPPE_VAR_1_17:KLEMMBÜGEL_6]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(7).translation = [395.81964916321505 82.31341493424236 253.62562081626902];  % mm
smiData.RigidTransform(7).angle = 2.0251096903870307;  % rad
smiData.RigidTransform(7).axis = [0.30430026295084045 -0.94558455365725191 -0.11520070248425228];
smiData.RigidTransform(7).ID = 'AssemblyGround[AKKUPLATTFORM_43:AKKU_BAUGRUPPE_VAR_1_17:ZUGBÜGEL_7]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(8).translation = [276.58738481787202 407.81855948920395 418.81057172686599];  % mm
smiData.RigidTransform(8).angle = 2.9458566544388392;  % rad
smiData.RigidTransform(8).axis = [-0.80588976306734783 -0.25934483157776833 0.53224237722784984];
smiData.RigidTransform(8).ID = 'AssemblyGround[AKKUPLATTFORM_43:AKKU_BAUGRUPPE_VAR_1_17:ZUGBÜGEL_8]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(9).translation = [419.48232018948801 386.09312103882394 118.42866020483399];  % mm
smiData.RigidTransform(9).angle = 2.2222714552285261;  % rad
smiData.RigidTransform(9).axis = [-0.42916196402397949 -0.83645121277397616 0.34083629103137636];
smiData.RigidTransform(9).ID = 'AssemblyGround[AKKUPLATTFORM_43:AKKU_BAUGRUPPE_VAR_1_17:ZUGBÜGEL_9]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(10).translation = [252.92471379188601 104.03885338384804 554.00753233790601];  % mm
smiData.RigidTransform(10).angle = 2.5207527405898249;  % rad
smiData.RigidTransform(10).axis = [0.78725683402431967 -0.40392157237427706 -0.46591205248705081];
smiData.RigidTransform(10).ID = 'AssemblyGround[AKKUPLATTFORM_43:AKKU_BAUGRUPPE_VAR_1_17:ZUGBÜGEL_10]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(11).translation = [330.352824871753 244.30745990989004 333.48971981962893];  % mm
smiData.RigidTransform(11).angle = 2.0251096903870529;  % rad
smiData.RigidTransform(11).axis = [0.30430026295085816 -0.94558455365724425 -0.11520070248426836];
smiData.RigidTransform(11).ID = 'AssemblyGround[AKKUPLATTFORM_43:AKKU_BAUGRUPPE_VAR_1_17:LAGERBOCK_11]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(12).translation = [258.11056717897304 242.04483772011699 304.40043065202696];  % mm
smiData.RigidTransform(12).angle = 2.0251096903870391;  % rad
smiData.RigidTransform(12).axis = [0.304300262950845 -0.9455845536572457 -0.11520070248429058];
smiData.RigidTransform(12).ID = 'AssemblyGround[AKKUPLATTFORM_43:AKKU_BAUGRUPPE_VAR_1_17:EINSATZ_12]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(13).translation = [378.75773949256893 332.68024280631897 189.63661598208995];  % mm
smiData.RigidTransform(13).angle = 2.5207527405898991;  % rad
smiData.RigidTransform(13).axis = [0.78725683402431534 -0.40392157237428061 -0.46591205248705492];
smiData.RigidTransform(13).ID = 'AssemblyGround[AKKUPLATTFORM_43:AKKU_BAUGRUPPE_VAR_1_17:AKKU_13]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(14).translation = [295.61578792896393 325.11261063220206 370.02895737571095];  % mm
smiData.RigidTransform(14).angle = 2.9458566544387836;  % rad
smiData.RigidTransform(14).axis = [-0.80588976306734139 -0.25934483157777682 0.53224237722785583];
smiData.RigidTransform(14).ID = 'AssemblyGround[AKKUPLATTFORM_43:AKKU_BAUGRUPPE_VAR_1_17:AKKU_14]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(15).translation = [353.38847757696203 161.985254583916 291.49372935966596];  % mm
smiData.RigidTransform(15).angle = 2.0251096903870618;  % rad
smiData.RigidTransform(15).axis = [0.30430026295084384 -0.94558455365724603 -0.11520070248429042];
smiData.RigidTransform(15).ID = 'AssemblyGround[AKKUPLATTFORM_43:AKKU_BAUGRUPPE_VAR_1_17:AKKU_15]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(16).translation = [270.24652601335498 154.41762240980199 471.88607075328599];  % mm
smiData.RigidTransform(16).angle = 2.222271455233229;  % rad
smiData.RigidTransform(16).axis = [-0.42916196402077045 -0.83645121277553236 0.34083629103159757];
smiData.RigidTransform(16).ID = 'AssemblyGround[AKKUPLATTFORM_43:AKKU_BAUGRUPPE_VAR_1_17:AKKU_16]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(17).translation = [-202.19487547715704 -147.169344622209 530.84493012305097];  % mm
smiData.RigidTransform(17).angle = 2.0320063219987885;  % rad
smiData.RigidTransform(17).axis = [0.080549774677357078 0.98793526175724977 -0.13227113205859098];
smiData.RigidTransform(17).ID = 'AssemblyGround[AKKUPLATTFORM_43:AKKU_BAUGRUPPE_VAR_1_17]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(18).translation = [150.801534788498 198.62869912955998 33.499999999999943];  % mm
smiData.RigidTransform(18).angle = 0;  % rad
smiData.RigidTransform(18).axis = [0 0 0];
smiData.RigidTransform(18).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_32:RECHTECKHOHLPROFIL_18]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(19).translation = [150.801534788498 198.62869912956 1.9999999999999574];  % mm
smiData.RigidTransform(19).angle = 0;  % rad
smiData.RigidTransform(19).axis = [0 0 0];
smiData.RigidTransform(19).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_32:DECKELA_19]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(20).translation = [150.801534788498 198.62869912956 64.999999999999972];  % mm
smiData.RigidTransform(20).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(20).axis = [0 1 0];
smiData.RigidTransform(20).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_32:BODEN_20]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(21).translation = [166.26600088256504 167.62869912956003 16.999999999999851];  % mm
smiData.RigidTransform(21).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(21).axis = [-1 -1.7225260246251598e-16 -4.9792002462515494e-17];
smiData.RigidTransform(21).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_32:SICHERUNG_21]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(22).translation = [166.26600088256504 199.62869912956 16.999999999999957];  % mm
smiData.RigidTransform(22).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(22).axis = [-1 -1.7225260246251598e-16 -4.9792002462515494e-17];
smiData.RigidTransform(22).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_32:SICHERUNG_22]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(23).translation = [166.26600088256504 231.62869912956 16.999999999999922];  % mm
smiData.RigidTransform(23).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(23).axis = [-1 -1.7225260246251598e-16 -4.9792002462515494e-17];
smiData.RigidTransform(23).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_32:SICHERUNG_23]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(24).translation = [184.80153478849809 153.62869912956003 46.999999999999957];  % mm
smiData.RigidTransform(24).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(24).axis = [-1 -1.7225260246251598e-16 -4.9792002462515494e-17];
smiData.RigidTransform(24).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_32:STECKERA_24]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(25).translation = [184.801534788498 185.62869912955995 46.999999999999922];  % mm
smiData.RigidTransform(25).angle = 1.5707963267948966;  % rad
smiData.RigidTransform(25).axis = [1 6.1230299999999957e-17 6.1230300000000154e-17];
smiData.RigidTransform(25).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_32:STECKERA_25]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(26).translation = [184.801534788498 217.62869912956 46.999999999999993];  % mm
smiData.RigidTransform(26).angle = 1.5707963267948966;  % rad
smiData.RigidTransform(26).axis = [1 6.1230299999999957e-17 6.1230300000000154e-17];
smiData.RigidTransform(26).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_32:STECKERA_26]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(27).translation = [187.80153478849803 248.62869912956 46.999999999999922];  % mm
smiData.RigidTransform(27).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(27).axis = [1.7225260000000009e-16 -1 -4.9791999999999792e-17];
smiData.RigidTransform(27).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_32:STECKER_B_27]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(28).translation = [122.30153478849802 244.12869912956 52.999999999999936];  % mm
smiData.RigidTransform(28).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(28).axis = [0.70710678118654746 0.70710678118654757 0];
smiData.RigidTransform(28).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_32:POM_KLOTZ_28]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(29).translation = [122.80153478849799 244.12869912956 52.999999999999936];  % mm
smiData.RigidTransform(29).angle = 2.0539125955565396e-15;  % rad
smiData.RigidTransform(29).axis = [0 -1 0];
smiData.RigidTransform(29).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_32:SECHSKANTSCHRAUBE - DIN 933 - M6  X 30_29]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(30).translation = [130.801534788498 244.12869912956 52.999999999999936];  % mm
smiData.RigidTransform(30).angle = 2.0539125955565396e-15;  % rad
smiData.RigidTransform(30).axis = [0 -1 0];
smiData.RigidTransform(30).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_32:SECHSKANTMUTTER - DIN 934 - M6_30]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(31).translation = [143.00153478849799 244.12869912956 52.999999999999936];  % mm
smiData.RigidTransform(31).angle = 0.81094220171830578;  % rad
smiData.RigidTransform(31).axis = [1 -2.6800834207200317e-15 -6.2575364930053685e-15];
smiData.RigidTransform(31).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_32:SECHSKANTMUTTER - DIN 934 - M6_31]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(32).translation = [-100.785866044253 -129.82086503203499 -126.12869912955901];  % mm
smiData.RigidTransform(32).angle = 2.593564245969481;  % rad
smiData.RigidTransform(32).axis = [0.28108463771482139 0.67859834454584855 0.67859834454584511];
smiData.RigidTransform(32).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_32]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(33).translation = [150.80153478849803 198.62869912956 33.5];  % mm
smiData.RigidTransform(33).angle = 0;  % rad
smiData.RigidTransform(33).axis = [0 0 0];
smiData.RigidTransform(33).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_B_40:RECHTECKHOHLPROFILA_33]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(34).translation = [150.80153478849803 198.62869912956 2.0000000000000018];  % mm
smiData.RigidTransform(34).angle = 0;  % rad
smiData.RigidTransform(34).axis = [0 0 0];
smiData.RigidTransform(34).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_B_40:DECKEL_34]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(35).translation = [150.80153478849803 198.62869912956 64.999999999999986];  % mm
smiData.RigidTransform(35).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(35).axis = [0 1 0];
smiData.RigidTransform(35).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_B_40:BODENA_35]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(36).translation = [166.26600088256504 167.62869912956003 16.999999999999929];  % mm
smiData.RigidTransform(36).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(36).axis = [-1 -1.7225260246251561e-16 -4.9792002462515642e-17];
smiData.RigidTransform(36).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_B_40:SICHERUNGA_36]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(37).translation = [184.801534788498 153.62869912956 47.000000000000014];  % mm
smiData.RigidTransform(37).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(37).axis = [1 6.1232349999999982e-17 0];
smiData.RigidTransform(37).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_B_40:STECKER_37]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(38).translation = [174.301534788498 228.62869912956 19.999999999999993];  % mm
smiData.RigidTransform(38).angle = 0;  % rad
smiData.RigidTransform(38).axis = [0 0 0];
smiData.RigidTransform(38).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_B_40:BUCHSE_BANANE_38]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(39).translation = [174.301534788498 228.62869912956 46.999999999999986];  % mm
smiData.RigidTransform(39).angle = 0;  % rad
smiData.RigidTransform(39).axis = [0 0 0];
smiData.RigidTransform(39).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_B_40:BUCHSE_BANANE_B_39]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(40).translation = [102.43445235005299 130.82086503203499 -126.12869912956];  % mm
smiData.RigidTransform(40).angle = 1.7177715174584018;  % rad
smiData.RigidTransform(40).axis = [0.86285620946101671 -0.35740674433659353 -0.35740674433659353];
smiData.RigidTransform(40).ID = 'AssemblyGround[AKKUPLATTFORM_43:BOX_FÜR_AKKUPLATTFORM_B_40]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(41).translation = [0.32429315290006144 -0.324293152900054 -11.500000000000004];  % mm
smiData.RigidTransform(41).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(41).axis = [0.70710678118654746 0.70710678118654757 0];
smiData.RigidTransform(41).ID = 'AssemblyGround[AKKUPLATTFORM_43:AKKUPLATTFORM_BUCHENMULTIPLEX_41]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(42).translation = [1.7763568394002505e-14 2.8449465006019636e-14 -23.000000000000007];  % mm
smiData.RigidTransform(42).angle = 0;  % rad
smiData.RigidTransform(42).axis = [0 0 0];
smiData.RigidTransform(42).ID = 'AssemblyGround[AKKUPLATTFORM_43:AKKUPLATTFORM_ALU_42]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(43).translation = [0 0 0];  % mm
smiData.RigidTransform(43).angle = 0;  % rad
smiData.RigidTransform(43).axis = [0 0 0];
smiData.RigidTransform(43).ID = 'AssemblyGround[RECHNERPLATTFORM_OHNE_HALTER_47:PLATTFORM_ALU_44]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(44).translation = [0 0 11.499999999999986];  % mm
smiData.RigidTransform(44).angle = 0;  % rad
smiData.RigidTransform(44).axis = [0 0 0];
smiData.RigidTransform(44).ID = 'AssemblyGround[RECHNERPLATTFORM_OHNE_HALTER_47:PLATTFORM_BUCHENMULTIPLESX_45]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(45).translation = [0 0 22.999999999999972];  % mm
smiData.RigidTransform(45).angle = 0;  % rad
smiData.RigidTransform(45).axis = [0 0 0];
smiData.RigidTransform(45).ID = 'AssemblyGround[RECHNERPLATTFORM_OHNE_HALTER_47:PLATTFORM_ALU_46]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(46).translation = [0 0 0];  % mm
smiData.RigidTransform(46).angle = 0;  % rad
smiData.RigidTransform(46).axis = [0 0 0];
smiData.RigidTransform(46).ID = 'AssemblyGround[KAPSELPLATTFORM_61:PLATTFORM_ALU-SCHICHT_48]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(47).translation = [139.79349497872701 146.994199896113 2.9999999999999716];  % mm
smiData.RigidTransform(47).angle = 0;  % rad
smiData.RigidTransform(47).axis = [0 0 0];
smiData.RigidTransform(47).ID = 'AssemblyGround[KAPSELPLATTFORM_61:PLATTFORM_HALTER_LINKS_51:HALTER_LINKS_49]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(48).translation = [139.79349497872701 149.994199896113 -1.0000000000000142];  % mm
smiData.RigidTransform(48).angle = 0;  % rad
smiData.RigidTransform(48).axis = [0 0 0];
smiData.RigidTransform(48).ID = 'AssemblyGround[KAPSELPLATTFORM_61:PLATTFORM_HALTER_LINKS_51:GUMMIAUFLAGE_50]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(49).translation = [-139.75349497872702 446.85419989611307 -4.5000000000000284];  % mm
smiData.RigidTransform(49).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(49).axis = [1 6.1232349999999995e-17 0];
smiData.RigidTransform(49).ID = 'AssemblyGround[KAPSELPLATTFORM_61:PLATTFORM_HALTER_LINKS_51]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(50).translation = [139.79349497872701 146.994199896113 2.9999999999999716];  % mm
smiData.RigidTransform(50).angle = 0;  % rad
smiData.RigidTransform(50).axis = [0 0 0];
smiData.RigidTransform(50).ID = 'AssemblyGround[KAPSELPLATTFORM_61:PLATTFORM_HALTER_RECHTS_54:HALTER_RECHTS_52]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(51).translation = [146.418314665427 149.99419989611306 -1.0000000000000142];  % mm
smiData.RigidTransform(51).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(51).axis = [-0 -1 -0];
smiData.RigidTransform(51).ID = 'AssemblyGround[KAPSELPLATTFORM_61:PLATTFORM_HALTER_RECHTS_54:GUMMIAUFLAGEAA_53]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(52).translation = [146.458314665427 -446.85419989611307 -4.5000000000000284];  % mm
smiData.RigidTransform(52).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(52).axis = [0 1 6.1230300000000006e-17];
smiData.RigidTransform(52).ID = 'AssemblyGround[KAPSELPLATTFORM_61:PLATTFORM_HALTER_RECHTS_54]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(53).translation = [139.79349497872704 146.994199896113 2.9999999999999716];  % mm
smiData.RigidTransform(53).angle = 0;  % rad
smiData.RigidTransform(53).axis = [0 0 0];
smiData.RigidTransform(53).ID = 'AssemblyGround[KAPSELPLATTFORM_61:PLATTFORM_HALTER_MITTE_57:HALTER_MITTE_55]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(54).translation = [146.418314665427 149.99419989611297 -1.0000000000000142];  % mm
smiData.RigidTransform(54).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(54).axis = [-0 -1 -0];
smiData.RigidTransform(54).ID = 'AssemblyGround[KAPSELPLATTFORM_61:PLATTFORM_HALTER_MITTE_57:GUMMIAUFLAGEA_56]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(55).translation = [-446.94419989611305 -146.418314665427 -4.5000000000000284];  % mm
smiData.RigidTransform(55).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(55).axis = [0.70710678118654746 0.70710678118654757 4.3296360344086661e-17];
smiData.RigidTransform(55).ID = 'AssemblyGround[KAPSELPLATTFORM_61:PLATTFORM_HALTER_MITTE_57]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(56).translation = [139.79349497872701 146.99419989611297 3.0000000000000426];  % mm
smiData.RigidTransform(56).angle = 0;  % rad
smiData.RigidTransform(56).axis = [0 0 0];
smiData.RigidTransform(56).ID = 'AssemblyGround[KAPSELPLATTFORM_61:PLATTFORM_HALTER_MITTE_58:HALTER_MITTE_55]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(57).translation = [146.418314665427 149.994199896113 -0.99999999999994316];  % mm
smiData.RigidTransform(57).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(57).axis = [-0 -1 -0];
smiData.RigidTransform(57).ID = 'AssemblyGround[KAPSELPLATTFORM_61:PLATTFORM_HALTER_MITTE_58:GUMMIAUFLAGEA_56]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(58).translation = [446.94419989611305 146.418314665427 -4.5000000000000284];  % mm
smiData.RigidTransform(58).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(58).axis = [0.70710678118654757 -0.70710678118654746 -4.3296360344086661e-17];
smiData.RigidTransform(58).ID = 'AssemblyGround[KAPSELPLATTFORM_61:PLATTFORM_HALTER_MITTE_58]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(59).translation = [1.7763568394002505e-14 0 11.499999999999986];  % mm
smiData.RigidTransform(59).angle = 0;  % rad
smiData.RigidTransform(59).axis = [0 0 0];
smiData.RigidTransform(59).ID = 'AssemblyGround[KAPSELPLATTFORM_61:PLATTFORM_BUCHEN-MULTIPLEX-SCHICHT_59]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(60).translation = [0 0 22.999999999999972];  % mm
smiData.RigidTransform(60).angle = 0;  % rad
smiData.RigidTransform(60).axis = [0 0 0];
smiData.RigidTransform(60).ID = 'AssemblyGround[KAPSELPLATTFORM_61:PLATTFORM_ALU-SCHICHT_60]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(61).translation = [0 0 0];  % mm
smiData.RigidTransform(61).angle = 0;  % rad
smiData.RigidTransform(61).axis = [0 0 0];
smiData.RigidTransform(61).ID = 'AssemblyGround[KAPSELPLATTFORM_62:PLATTFORM_ALU-SCHICHT_48]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(62).translation = [139.79349497872701 146.99419989611297 3.0000000000001137];  % mm
smiData.RigidTransform(62).angle = 0;  % rad
smiData.RigidTransform(62).axis = [0 0 0];
smiData.RigidTransform(62).ID = 'AssemblyGround[KAPSELPLATTFORM_62:PLATTFORM_HALTER_LINKS_51:HALTER_LINKS_49]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(63).translation = [139.79349497872701 149.99419989611297 -0.99999999999994316];  % mm
smiData.RigidTransform(63).angle = 0;  % rad
smiData.RigidTransform(63).axis = [0 0 0];
smiData.RigidTransform(63).ID = 'AssemblyGround[KAPSELPLATTFORM_62:PLATTFORM_HALTER_LINKS_51:GUMMIAUFLAGE_50]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(64).translation = [-139.75349497872702 446.85419989611313 -4.4999999999998863];  % mm
smiData.RigidTransform(64).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(64).axis = [1 6.1232349999999995e-17 0];
smiData.RigidTransform(64).ID = 'AssemblyGround[KAPSELPLATTFORM_62:PLATTFORM_HALTER_LINKS_51]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(65).translation = [139.79349497872701 146.99419989611297 3.0000000000001137];  % mm
smiData.RigidTransform(65).angle = 0;  % rad
smiData.RigidTransform(65).axis = [0 0 0];
smiData.RigidTransform(65).ID = 'AssemblyGround[KAPSELPLATTFORM_62:PLATTFORM_HALTER_RECHTS_54:HALTER_RECHTS_52]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(66).translation = [146.418314665427 149.994199896113 -0.99999999999994316];  % mm
smiData.RigidTransform(66).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(66).axis = [-0 -1 -0];
smiData.RigidTransform(66).ID = 'AssemblyGround[KAPSELPLATTFORM_62:PLATTFORM_HALTER_RECHTS_54:GUMMIAUFLAGEAA_53]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(67).translation = [146.458314665427 -446.85419989611296 -4.4999999999998863];  % mm
smiData.RigidTransform(67).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(67).axis = [0 1 6.1230300000000006e-17];
smiData.RigidTransform(67).ID = 'AssemblyGround[KAPSELPLATTFORM_62:PLATTFORM_HALTER_RECHTS_54]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(68).translation = [139.79349497872704 146.994199896113 3.0000000000001137];  % mm
smiData.RigidTransform(68).angle = 0;  % rad
smiData.RigidTransform(68).axis = [0 0 0];
smiData.RigidTransform(68).ID = 'AssemblyGround[KAPSELPLATTFORM_62:PLATTFORM_HALTER_MITTE_57:HALTER_MITTE_55]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(69).translation = [146.418314665427 149.99419989611297 -0.99999999999994316];  % mm
smiData.RigidTransform(69).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(69).axis = [-0 -1 -0];
smiData.RigidTransform(69).ID = 'AssemblyGround[KAPSELPLATTFORM_62:PLATTFORM_HALTER_MITTE_57:GUMMIAUFLAGEA_56]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(70).translation = [-446.94419989611305 -146.418314665427 -4.4999999999998863];  % mm
smiData.RigidTransform(70).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(70).axis = [0.70710678118654746 0.70710678118654757 4.3296360344086661e-17];
smiData.RigidTransform(70).ID = 'AssemblyGround[KAPSELPLATTFORM_62:PLATTFORM_HALTER_MITTE_57]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(71).translation = [139.79349497872701 146.99419989611297 3.0000000000001137];  % mm
smiData.RigidTransform(71).angle = 0;  % rad
smiData.RigidTransform(71).axis = [0 0 0];
smiData.RigidTransform(71).ID = 'AssemblyGround[KAPSELPLATTFORM_62:PLATTFORM_HALTER_MITTE_58:HALTER_MITTE_55]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(72).translation = [146.418314665427 149.994199896113 -0.99999999999994316];  % mm
smiData.RigidTransform(72).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(72).axis = [-0 -1 -0];
smiData.RigidTransform(72).ID = 'AssemblyGround[KAPSELPLATTFORM_62:PLATTFORM_HALTER_MITTE_58:GUMMIAUFLAGEA_56]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(73).translation = [446.94419989611305 146.418314665427 -4.4999999999998863];  % mm
smiData.RigidTransform(73).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(73).axis = [0.70710678118654757 -0.70710678118654746 -4.3296360344086661e-17];
smiData.RigidTransform(73).ID = 'AssemblyGround[KAPSELPLATTFORM_62:PLATTFORM_HALTER_MITTE_58]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(74).translation = [1.7763568394002505e-14 0 11.500000000000057];  % mm
smiData.RigidTransform(74).angle = 0;  % rad
smiData.RigidTransform(74).axis = [0 0 0];
smiData.RigidTransform(74).ID = 'AssemblyGround[KAPSELPLATTFORM_62:PLATTFORM_BUCHEN-MULTIPLEX-SCHICHT_59]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(75).translation = [0 0 23.000000000000114];  % mm
smiData.RigidTransform(75).angle = 0;  % rad
smiData.RigidTransform(75).axis = [0 0 0];
smiData.RigidTransform(75).ID = 'AssemblyGround[KAPSELPLATTFORM_62:PLATTFORM_ALU-SCHICHT_60]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(76).translation = [212.24857873846506 159.44955711508899 625];  % mm
smiData.RigidTransform(76).angle = 0;  % rad
smiData.RigidTransform(76).axis = [0 0 0];
smiData.RigidTransform(76).ID = 'AssemblyGround[STRINGER_60X90_1341_64:STRINGER_1_63]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(77).translation = [212.24857873846503 159.44955711508899 625];  % mm
smiData.RigidTransform(77).angle = 0;  % rad
smiData.RigidTransform(77).axis = [0 0 0];
smiData.RigidTransform(77).ID = 'AssemblyGround[STRINGER_60X90_1341_65:STRINGER_1_63]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(78).translation = [212.248578738465 159.44955711508899 625];  % mm
smiData.RigidTransform(78).angle = 0;  % rad
smiData.RigidTransform(78).axis = [0 0 0];
smiData.RigidTransform(78).ID = 'AssemblyGround[STRINGER_60X90_1341_66:STRINGER_1_63]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(79).translation = [212.24857873846503 159.44955711508902 625];  % mm
smiData.RigidTransform(79).angle = 0;  % rad
smiData.RigidTransform(79).axis = [0 0 0];
smiData.RigidTransform(79).ID = 'AssemblyGround[STRINGER_60X90_1341_67:STRINGER_1_63]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(80).translation = [72.717857067209977 152.41475248568301 5.9999999999999964];  % mm
smiData.RigidTransform(80).angle = 0;  % rad
smiData.RigidTransform(80).axis = [0 0 0];
smiData.RigidTransform(80).ID = 'AssemblyGround[SENSORKLOTZ_71:SENSORKLOTZA_70]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(81).translation = [165.83360533307601 2.7580216480265562 0];  % mm
smiData.RigidTransform(81).angle = 0;  % rad
smiData.RigidTransform(81).axis = [0 0 0];
smiData.RigidTransform(81).ID = 'AssemblyGround[KAPSEL_SPANNBAND_74:SPANNKLOTZ_72]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(82).translation = [165.83360533307601 2.7580216480265562 0];  % mm
smiData.RigidTransform(82).angle = 0;  % rad
smiData.RigidTransform(82).axis = [0 0 0];
smiData.RigidTransform(82).ID = 'AssemblyGround[KAPSEL_SPANNBAND_74:SPANNBAND_73]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(83).translation = [165.83360533307601 2.7580216480265562 0];  % mm
smiData.RigidTransform(83).angle = 0;  % rad
smiData.RigidTransform(83).axis = [0 0 0];
smiData.RigidTransform(83).ID = 'AssemblyGround[KAPSEL_SPANNBAND_75:SPANNKLOTZ_72]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(84).translation = [165.83360533307601 2.7580216480265562 0];  % mm
smiData.RigidTransform(84).angle = 0;  % rad
smiData.RigidTransform(84).axis = [0 0 0];
smiData.RigidTransform(84).ID = 'AssemblyGround[KAPSEL_SPANNBAND_75:SPANNBAND_73]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(85).translation = [233.13836100337903 48.419722719209602 0];  % mm
smiData.RigidTransform(85).angle = 0;  % rad
smiData.RigidTransform(85).axis = [0 0 0];
smiData.RigidTransform(85).ID = 'AssemblyGround[KEGEL_05A_77:KEGEL_05_76]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(86).translation = [0 0 0];  % mm
smiData.RigidTransform(86).angle = 0;  % rad
smiData.RigidTransform(86).axis = [0 0 0];
smiData.RigidTransform(86).ID = 'AssemblyGround[KONTAKTPLATTEA_79:KONTAKTPLATTE_78]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(87).translation = [0 0 0];  % mm
smiData.RigidTransform(87).angle = 0;  % rad
smiData.RigidTransform(87).axis = [0 0 0];
smiData.RigidTransform(87).ID = 'AssemblyGround[FUSS_KATAPULT_KAPSEL_SENSOR_81:FUSS_KATAPULT_KAPSEL_SENSORA_80]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(88).translation = [0 0 0];  % mm
smiData.RigidTransform(88).angle = 0;  % rad
smiData.RigidTransform(88).axis = [0 0 0];
smiData.RigidTransform(88).ID = 'AssemblyGround[FUSS_KATAPULT_KAPSEL_REV_2_83:FUSS_KATAPULT_KAPSEL_REV_2A_82]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(89).translation = [0 0 0];  % mm
smiData.RigidTransform(89).angle = 0;  % rad
smiData.RigidTransform(89).axis = [0 0 0];
smiData.RigidTransform(89).ID = 'AssemblyGround[FUSS_KATAPULT_KAPSEL_REV_2_84:FUSS_KATAPULT_KAPSEL_REV_2A_82]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(90).translation = [0 0 0];  % mm
smiData.RigidTransform(90).angle = 0;  % rad
smiData.RigidTransform(90).axis = [0 0 0];
smiData.RigidTransform(90).ID = 'AssemblyGround[FUSS_KATAPULT_KAPSEL_REV_2_85:FUSS_KATAPULT_KAPSEL_REV_2A_82]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(91).translation = [0 27.5 0];  % mm
smiData.RigidTransform(91).angle = 0;  % rad
smiData.RigidTransform(91).axis = [0 0 0];
smiData.RigidTransform(91).ID = 'AssemblyGround[STANGE_FUER_KAPSELSPITZE_2_89:GEWINDESTUECK_OBEN_86]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(92).translation = [17.281218828155005 -90.751840587833698 0];  % mm
smiData.RigidTransform(92).angle = 0;  % rad
smiData.RigidTransform(92).axis = [0 0 0];
smiData.RigidTransform(92).ID = 'AssemblyGround[STANGE_FUER_KAPSELSPITZE_2_89:MITTELSTUECK_87]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(93).translation = [0 -366.35035620354301 0];  % mm
smiData.RigidTransform(93).angle = 0;  % rad
smiData.RigidTransform(93).axis = [0 0 0];
smiData.RigidTransform(93).ID = 'AssemblyGround[STANGE_FUER_KAPSELSPITZE_2_89:ZENTRIERSTUECK_UNTEN_88]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(94).translation = [0 37.647658983217191 0];  % mm
smiData.RigidTransform(94).angle = 0;  % rad
smiData.RigidTransform(94).axis = [0 0 0];
smiData.RigidTransform(94).ID = 'AssemblyGround[FALLKAPSEL_BODEN_080610_95:FALLKAPSEL_BODEN_080610A_94]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(95).translation = [0 30 0];  % mm
smiData.RigidTransform(95).angle = 0;  % rad
smiData.RigidTransform(95).axis = [0 0 0];
smiData.RigidTransform(95).ID = 'AssemblyGround[FALLKAPSEL_DECKEL_080708_104:KAPSEL_DECKEL_96]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(96).translation = [1.7763568394002505e-13 52.616349032421112 -330];  % mm
smiData.RigidTransform(96).angle = 0;  % rad
smiData.RigidTransform(96).axis = [0 0 0];
smiData.RigidTransform(96).ID = 'AssemblyGround[FALLKAPSEL_DECKEL_080708_104:SICHERUNGSHUELSE_97]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(97).translation = [-1.2434497875801753e-13 52.616349032421112 330];  % mm
smiData.RigidTransform(97).angle = 0;  % rad
smiData.RigidTransform(97).axis = [0 0 0];
smiData.RigidTransform(97).ID = 'AssemblyGround[FALLKAPSEL_DECKEL_080708_104:SICHERUNGSHUELSE_98]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(98).translation = [193.98344704189401 -1.5000000000000568 90.139388094253107];  % mm
smiData.RigidTransform(98).angle = 0;  % rad
smiData.RigidTransform(98).axis = [0 0 0];
smiData.RigidTransform(98).ID = 'AssemblyGround[FALLKAPSEL_DECKEL_080708_104:ABDECKBLECH_99]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(99).translation = [87.442631645126028 34.999999999998579 73.410254037843899];  % mm
smiData.RigidTransform(99).angle = 1.5957776918654869;  % rad
smiData.RigidTransform(99).axis = [-0.41319285921189786 0.56310447022305266 -0.71567102547966266];
smiData.RigidTransform(99).ID = 'AssemblyGround[FALLKAPSEL_DECKEL_080708_104:BLITZSCHUTZ_100]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(100).translation = [74.910254037844396 15 89.720199780790196];  % mm
smiData.RigidTransform(100).angle = 0.62705701126657243;  % rad
smiData.RigidTransform(100).axis = [-0.14572299516526896 0.82643617318109297 -0.5438456217878872];
smiData.RigidTransform(100).ID = 'AssemblyGround[FALLKAPSEL_DECKEL_080708_104:DRUCKSENSOR_101]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(101).translation = [90.660254037844481 15 117.00000000000001];  % mm
smiData.RigidTransform(101).angle = 3.1415926535897913;  % rad
smiData.RigidTransform(101).axis = [-0.25881904510252129 -3.0171426054391121e-16 -0.96592582628906809];
smiData.RigidTransform(101).ID = 'AssemblyGround[FALLKAPSEL_DECKEL_080708_104:HALTER_1_102]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(102).translation = [100 35 80.660254037844297];  % mm
smiData.RigidTransform(102).angle = 3.1415926535897927;  % rad
smiData.RigidTransform(102).axis = [-0.86602540378443837 5.4123377255540933e-16 0.50000000000000056];
smiData.RigidTransform(102).ID = 'AssemblyGround[FALLKAPSEL_DECKEL_080708_104:HALTER_2_103]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(103).translation = [195.75940717518702 149.97713976088599 0];  % mm
smiData.RigidTransform(103).angle = 0;  % rad
smiData.RigidTransform(103).axis = [0 0 0];
smiData.RigidTransform(103).ID = 'AssemblyGround[ANDOCKSYSTEM_KAPSELSEITIG_107:DECKPLATTE_ANDOCKSYSTEM_105]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(104).translation = [304.30157438001203 145.86343318229703 -15];  % mm
smiData.RigidTransform(104).angle = 0;  % rad
smiData.RigidTransform(104).axis = [0 0 0];
smiData.RigidTransform(104).ID = 'AssemblyGround[ANDOCKSYSTEM_KAPSELSEITIG_107:AUSRICHTHILFE_106]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(105).translation = [180.44378652744297 94.170430769371833 10];  % mm
smiData.RigidTransform(105).angle = 0;  % rad
smiData.RigidTransform(105).axis = [0 0 0];
smiData.RigidTransform(105).ID = 'AssemblyGround[STRINGERFUSS_VAR_2_REV_2_109:STRINGERFUSS_VAR_2_REV_2A_108]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(106).translation = [180.443786527443 94.170430769371833 10];  % mm
smiData.RigidTransform(106).angle = 0;  % rad
smiData.RigidTransform(106).axis = [0 0 0];
smiData.RigidTransform(106).ID = 'AssemblyGround[STRINGERFUSS_VAR_2_REV_2_110:STRINGERFUSS_VAR_2_REV_2A_108]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(107).translation = [180.443786527443 94.170430769371833 10];  % mm
smiData.RigidTransform(107).angle = 0;  % rad
smiData.RigidTransform(107).axis = [0 0 0];
smiData.RigidTransform(107).ID = 'AssemblyGround[STRINGERFUSS_VAR_2_REV_2_111:STRINGERFUSS_VAR_2_REV_2A_108]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(108).translation = [180.443786527443 94.170430769371833 10];  % mm
smiData.RigidTransform(108).angle = 0;  % rad
smiData.RigidTransform(108).axis = [0 0 0];
smiData.RigidTransform(108).ID = 'AssemblyGround[STRINGERFUSS_VAR_2_REV_2_112:STRINGERFUSS_VAR_2_REV_2A_108]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(109).translation = [0 0 10.999999999999943];  % mm
smiData.RigidTransform(109).angle = 0;  % rad
smiData.RigidTransform(109).axis = [0 0 0];
smiData.RigidTransform(109).ID = 'AssemblyGround[BERGEBUEGEL_05_121:MITTELTEIL_113]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(110).translation = [-105.37538971406406 -29.139540144372305 11.443743270357629];  % mm
smiData.RigidTransform(110).angle = 2.2204460492503131e-16;  % rad
smiData.RigidTransform(110).axis = [0 -1 0];
smiData.RigidTransform(110).ID = 'AssemblyGround[BERGEBUEGEL_05_121:SCHWEIßBAUGRUPPE_117:VERBINDUNGSSTUECK_114]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(111).translation = [68.624610285936427 -32.5895401443724 14.893743270357618];  % mm
smiData.RigidTransform(111).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(111).axis = [-1.1102229999999998e-16 1 -1.1102229999999996e-16];
smiData.RigidTransform(111).ID = 'AssemblyGround[BERGEBUEGEL_05_121:SCHWEIßBAUGRUPPE_117:ROHR_115]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(112).translation = [210.8946102859359 -32.5895401443724 14.893743270357618];  % mm
smiData.RigidTransform(112).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(112).axis = [5.5511150000000047e-17 -1 -1.6653344999999996e-16];
smiData.RigidTransform(112).ID = 'AssemblyGround[BERGEBUEGEL_05_121:SCHWEIßBAUGRUPPE_117:GEWINDESTUECK_116]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(113).translation = [-186.15402379006503 32.589540144372393 -157.23812993142587];  % mm
smiData.RigidTransform(113).angle = 0.78539816339744606;  % rad
smiData.RigidTransform(113).axis = [4.3606303477171048e-16 -1 4.634660769479883e-16];
smiData.RigidTransform(113).ID = 'AssemblyGround[BERGEBUEGEL_05_121:SCHWEIßBAUGRUPPE_117]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(114).translation = [-105.37538971406406 -29.139540144372305 11.443743270357629];  % mm
smiData.RigidTransform(114).angle = 1.1102230246251565e-16;  % rad
smiData.RigidTransform(114).axis = [0 -1 0];
smiData.RigidTransform(114).ID = 'AssemblyGround[BERGEBUEGEL_05_121:SCHWEIßBAUGRUPPE_118:VERBINDUNGSSTUECK_114]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(115).translation = [68.624610285936427 -32.5895401443724 14.893743270357902];  % mm
smiData.RigidTransform(115).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(115).axis = [-1.1102230000000014e-16 1 -1.1102229999999993e-16];
smiData.RigidTransform(115).ID = 'AssemblyGround[BERGEBUEGEL_05_121:SCHWEIßBAUGRUPPE_118:ROHR_115]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(116).translation = [210.89461028593618 -32.589540144372378 14.893743270357618];  % mm
smiData.RigidTransform(116).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(116).axis = [5.5511149999999967e-17 -1 -1.6653345000000033e-16];
smiData.RigidTransform(116).ID = 'AssemblyGround[BERGEBUEGEL_05_121:SCHWEIßBAUGRUPPE_118:GEWINDESTUECK_116]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(117).translation = [-32.589540144371881 -186.154023790065 -157.23812993142502];  % mm
smiData.RigidTransform(117).angle = 1.7177715174584045;  % rad
smiData.RigidTransform(117).axis = [0.35740674433659275 -0.35740674433659164 0.86285620946101771];
smiData.RigidTransform(117).ID = 'AssemblyGround[BERGEBUEGEL_05_121:SCHWEIßBAUGRUPPE_118]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(118).translation = [-105.37538971406391 -29.139540144372305 11.443743270357771];  % mm
smiData.RigidTransform(118).angle = 2.2204460492503136e-16;  % rad
smiData.RigidTransform(118).axis = [0 -1 0];
smiData.RigidTransform(118).ID = 'AssemblyGround[BERGEBUEGEL_05_121:SCHWEIßBAUGRUPPE_119:VERBINDUNGSSTUECK_114]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(119).translation = [68.624610285936569 -32.5895401443724 14.89374327035776];  % mm
smiData.RigidTransform(119).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(119).axis = [-1.1102229999999993e-16 1 -1.1102230000000004e-16];
smiData.RigidTransform(119).ID = 'AssemblyGround[BERGEBUEGEL_05_121:SCHWEIßBAUGRUPPE_119:ROHR_115]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(120).translation = [210.89461028593604 -32.589540144372407 14.893743270357476];  % mm
smiData.RigidTransform(120).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(120).axis = [5.551115000000017e-17 -1 -1.6653344999999991e-16];
smiData.RigidTransform(120).ID = 'AssemblyGround[BERGEBUEGEL_05_121:SCHWEIßBAUGRUPPE_119:GEWINDESTUECK_116]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(121).translation = [186.15402379006503 -32.589540144372009 -157.23812993142616];  % mm
smiData.RigidTransform(121).angle = 3.1415926535897909;  % rad
smiData.RigidTransform(121).axis = [-0.38268343236508945 -3.2924266889848296e-16 -0.92387953251128685];
smiData.RigidTransform(121).ID = 'AssemblyGround[BERGEBUEGEL_05_121:SCHWEIßBAUGRUPPE_119]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(122).translation = [-105.37538971406406 -29.139540144372305 11.443743270357629];  % mm
smiData.RigidTransform(122).angle = 2.2204460492503131e-16;  % rad
smiData.RigidTransform(122).axis = [0 -1 0];
smiData.RigidTransform(122).ID = 'AssemblyGround[BERGEBUEGEL_05_121:SCHWEIßBAUGRUPPE_120:VERBINDUNGSSTUECK_114]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(123).translation = [68.624610285936285 -32.5895401443724 14.893743270357902];  % mm
smiData.RigidTransform(123).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(123).axis = [-1.1102229999999993e-16 1 -1.1102229999999993e-16];
smiData.RigidTransform(123).ID = 'AssemblyGround[BERGEBUEGEL_05_121:SCHWEIßBAUGRUPPE_120:ROHR_115]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(124).translation = [210.89461028593576 -32.589540144372421 14.893743270357618];  % mm
smiData.RigidTransform(124).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(124).axis = [5.5511149999999776e-17 -1 -1.6653344999999991e-16];
smiData.RigidTransform(124).ID = 'AssemblyGround[BERGEBUEGEL_05_121:SCHWEIßBAUGRUPPE_120:GEWINDESTUECK_116]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(125).translation = [32.589540144371902 186.15402379006503 -157.23812993142616];  % mm
smiData.RigidTransform(125).angle = 1.7177715174583981;  % rad
smiData.RigidTransform(125).axis = [-0.35740674433659309 -0.35740674433659403 -0.8628562094610166];
smiData.RigidTransform(125).ID = 'AssemblyGround[BERGEBUEGEL_05_121:SCHWEIßBAUGRUPPE_120]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(126).translation = [-144.69736457531201 165.15347379075001 0.5];  % mm
smiData.RigidTransform(126).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(126).axis = [-1 -6.1232349999999982e-17 -6.1232349999999982e-17];
smiData.RigidTransform(126).ID = 'RootGround[AKKUPLATTFORM_43]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(127).translation = [-144.69736457531201 334.15347379075001 0.49999999999991301];  % mm
smiData.RigidTransform(127).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(127).axis = [-1 -6.1232349999999982e-17 -6.1232349999999982e-17];
smiData.RigidTransform(127).ID = 'SixDofRigidTransform[RECHNERPLATTFORM_OHNE_HALTER_47]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(128).translation = [-143.87307142241201 590.15347379075001 -6.256107000000001e-14];  % mm
smiData.RigidTransform(128).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(128).axis = [-1 -6.1232349999999982e-17 -6.1232349999999982e-17];
smiData.RigidTransform(128).ID = 'SixDofRigidTransform[KAPSELPLATTFORM_61]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(129).translation = [-143.87307142241201 1515.15347379075 -1.211661e-13];  % mm
smiData.RigidTransform(129).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(129).axis = [-1 -6.1232349999999982e-17 -6.1232349999999982e-17];
smiData.RigidTransform(129).ID = 'SixDofRigidTransform[KAPSELPLATTFORM_62]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(130).translation = [15.5764856926767 328.65347379075001 -117.751421261535];  % mm
smiData.RigidTransform(130).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(130).axis = [-0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(130).ID = 'SixDofRigidTransform[STRINGER_60X90_1341_64]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(131).translation = [-26.121650160876904 328.65347379075001 159.449557115088];  % mm
smiData.RigidTransform(131).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(131).axis = [-1 -6.1232349999999982e-17 -6.1232349999999982e-17];
smiData.RigidTransform(131).ID = 'SixDofRigidTransform[STRINGER_60X90_1341_65]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(132).translation = [-303.32262853750001 328.65347379075001 117.751421261535];  % mm
smiData.RigidTransform(132).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(132).axis = [-0.57735026918962573 -0.57735026918962584 -0.57735026918962573];
smiData.RigidTransform(132).ID = 'SixDofRigidTransform[STRINGER_60X90_1341_66]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(133).translation = [-686.12165016087704 328.65347379075001 159.44955711508899];  % mm
smiData.RigidTransform(133).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(133).axis = [-1 -6.1232349999999982e-17 -6.1232349999999982e-17];
smiData.RigidTransform(133).ID = 'SixDofRigidTransform[STRINGER_60X90_1341_67]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(134).translation = [-190.11606952611899 -94.761278694932599 -253.536360702894];  % mm
smiData.RigidTransform(134).angle = 1.6929693744345051;  % rad
smiData.RigidTransform(134).axis = [-8.5752067233081432e-15 1 -4.5931047702115255e-15];
smiData.RigidTransform(134).ID = 'SixDofRigidTransform[SENSORKLOTZ_71]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(135).translation = [-309.70667675548799 1664.0954521427202 -9.1453730000000002e-14];  % mm
smiData.RigidTransform(135).angle = 1.332268e-15;  % rad
smiData.RigidTransform(135).axis = [1 0 0];
smiData.RigidTransform(135).ID = 'SixDofRigidTransform[KAPSEL_SPANNBAND_74]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(136).translation = [-309.70667675548799 133.69545214272301 -6.2066549999999997e-14];  % mm
smiData.RigidTransform(136).angle = 0;  % rad
smiData.RigidTransform(136).axis = [0 0 0];
smiData.RigidTransform(136).ID = 'SixDofRigidTransform[KAPSEL_SPANNBAND_75]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(137).translation = [-377.01143242579099 -203.880295029156 -6.2066549999999997e-14];  % mm
smiData.RigidTransform(137).angle = 0;  % rad
smiData.RigidTransform(137).axis = [0 0 0];
smiData.RigidTransform(137).ID = 'SixDofRigidTransform[KEGEL_05A_77]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(138).translation = [-143.87307142241201 -374.09652620924999 -2.1316279999999999e-14];  % mm
smiData.RigidTransform(138).angle = 0.52359877559832557;  % rad
smiData.RigidTransform(138).axis = [-2.164935105954252e-15 1 -5.1792613866934755e-15];
smiData.RigidTransform(138).ID = 'SixDofRigidTransform[KONTAKTPLATTEA_79]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(139).translation = [-129.22016039951899 99.653473790750297 -317.16135464809997];  % mm
smiData.RigidTransform(139).angle = 2.094395102393197;  % rad
smiData.RigidTransform(139).axis = [0.57735026918962673 -0.57735026918962418 0.57735026918962629];
smiData.RigidTransform(139).ID = 'SixDofRigidTransform[FUSS_KATAPULT_KAPSEL_SENSOR_81]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(140).translation = [173.28828322568799 99.653473790749999 14.6529110228931];  % mm
smiData.RigidTransform(140).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(140).axis = [4.3297809912988083e-17 -0.70710678118654746 0.70710678118654757];
smiData.RigidTransform(140).ID = 'SixDofRigidTransform[FUSS_KATAPULT_KAPSEL_REV_2_83]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(141).translation = [-158.525982445305 99.653473790749999 317.16135464809901];  % mm
smiData.RigidTransform(141).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(141).axis = [0.57735026918962573 0.57735026918962584 -0.57735026918962573];
smiData.RigidTransform(141).ID = 'SixDofRigidTransform[FUSS_KATAPULT_KAPSEL_REV_2_84]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(142).translation = [-461.03442607051107 99.653473790749999 -14.652911022893299];  % mm
smiData.RigidTransform(142).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(142).axis = [1 0 0];
smiData.RigidTransform(142).ID = 'SixDofRigidTransform[FUSS_KATAPULT_KAPSEL_REV_2_85]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(143).translation = [-143.87307142241201 18.653473790749999 -6.2066549999999997e-14];  % mm
smiData.RigidTransform(143).angle = 0;  % rad
smiData.RigidTransform(143).axis = [0 0 0];
smiData.RigidTransform(143).ID = 'SixDofRigidTransform[STANGE_FUER_KAPSELSPITZE_2_89]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(144).translation = [-143.87307142241201 54.653473790750098 -6.2066549999999997e-14];  % mm
smiData.RigidTransform(144).angle = 1.082467e-15;  % rad
smiData.RigidTransform(144).axis = [0 1 0];
smiData.RigidTransform(144).ID = 'SixDofRigidTransform[FALLKAPSEL_BODEN_080610_95]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(145).translation = [-143.87307142241201 1630.65347379075 -6.2066549999999997e-14];  % mm
smiData.RigidTransform(145).angle = 3.6738600000000002e-16;  % rad
smiData.RigidTransform(145).axis = [0 1 0];
smiData.RigidTransform(145).ID = 'SixDofRigidTransform[FALLKAPSEL_DECKEL_080708_104]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(146).translation = [16.785855569343799 1695.65347379075 -45.623118468950906];  % mm
smiData.RigidTransform(146).angle = 1.7177715174584018;  % rad
smiData.RigidTransform(146).axis = [0.86285620946101671 -0.35740674433659353 0.35740674433659353];
smiData.RigidTransform(146).ID = 'SixDofRigidTransform[ANDOCKSYSTEM_KAPSELSEITIG_107]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(147).translation = [-324.31685794985498 127.65347379075001 -220.82956923062801];  % mm
smiData.RigidTransform(147).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(147).axis = [-1 -6.1232349999999982e-17 -6.1232349999999982e-17];
smiData.RigidTransform(147).ID = 'SixDofRigidTransform[STRINGERFUSS_VAR_2_REV_2_109]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(148).translation = [76.956497808216511 127.65347379075001 -180.44378652744399];  % mm
smiData.RigidTransform(148).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(148).axis = [-0.57735026918962595 -0.57735026918962562 -0.57735026918962562];
smiData.RigidTransform(148).ID = 'SixDofRigidTransform[STRINGERFUSS_VAR_2_REV_2_110]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(149).translation = [36.570715105031802 127.65347379075001 220.82956923062801];  % mm
smiData.RigidTransform(149).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(149).axis = [0 0.70710678118654746 0.70710678118654757];
smiData.RigidTransform(149).ID = 'SixDofRigidTransform[STRINGERFUSS_VAR_2_REV_2_111]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(150).translation = [-364.70264065304002 127.65347379075001 180.443786527443];  % mm
smiData.RigidTransform(150).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(150).axis = [-0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(150).ID = 'SixDofRigidTransform[STRINGERFUSS_VAR_2_REV_2_112]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(151).translation = [-143.87307142241201 1923.7962321310301 -9.4453210000000007e-14];  % mm
smiData.RigidTransform(151).angle = 1.570796326794897;  % rad
smiData.RigidTransform(151).axis = [-1 9.8101850000000006e-17 -2.2056654999999986e-16];
smiData.RigidTransform(151).ID = 'SixDofRigidTransform[BERGEBUEGEL_05_121]';


%============= Solid =============%
%Center of Mass (CoM) %Moments of Inertia (MoI) %Product of Inertia (PoI)

%Initialize the Solid structure array by filling in null values.
smiData.Solid(60).mass = 0.0;
smiData.Solid(60).CoM = [0.0 0.0 0.0];
smiData.Solid(60).MoI = [0.0 0.0 0.0];
smiData.Solid(60).PoI = [0.0 0.0 0.0];
smiData.Solid(60).color = [0.0 0.0 0.0];
smiData.Solid(60).opacity = 0.0;
smiData.Solid(60).ID = '';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(1).mass = 3.6683893418212663;  % lbm
smiData.Solid(1).CoM = [0.82429315289643945 0.50000000000007516 0];  % mm
smiData.Solid(1).MoI = [99118.150266084063 99118.150266039156 198221.01557653226];  % lbm*mm^2
smiData.Solid(1).PoI = [0 0 -61.047938403898243];  % lbm*mm^2
smiData.Solid(1).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(1).opacity = 1;
smiData.Solid(1).ID = 'AKKUPLATTFORM_ALU.ipt_{FA9DD285-4B67-0B28-301B-8C98CF41AF29}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(2).mass = 1.7755178295313074;  % lbm
smiData.Solid(2).CoM = [-4.6527167172289856e-12 1.3551602089016463e-13 -1.7043531739092965];  % mm
smiData.Solid(2).MoI = [12831.991390170262 32122.349462589264 44939.07915459598];  % lbm*mm^2
smiData.Solid(2).PoI = [0 2.4500929722274209e-12 1.2832592601472181e-11];  % lbm*mm^2
smiData.Solid(2).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(2).opacity = 1;
smiData.Solid(2).ID = 'GRUNDPLATTE_GROSS.ipt_{CC61710B-471D-F63D-5751-F39099547542}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(3).mass = 0.11845869470457578;  % lbm
smiData.Solid(3).CoM = [-1.9999999999999853 18.046492793003619 1.4035059156533616];  % mm
smiData.Solid(3).MoI = [367.83771094498388 14.604603329031322 378.44568413342125];  % lbm*mm^2
smiData.Solid(3).PoI = [-0.038615131911884558 0 0];  % lbm*mm^2
smiData.Solid(3).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(3).opacity = 1;
smiData.Solid(3).ID = 'KLEMMBÜGEL.ipt_{4F54D175-4CA8-538B-D383-F6964BB283F6}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(4).mass = 0.070433734768651668;  % lbm
smiData.Solid(4).CoM = [-4.2746162162742786e-12 0 18.940209841321998];  % mm
smiData.Solid(4).MoI = [152.94692718260353 161.65708268181461 11.024408576641461];  % lbm*mm^2
smiData.Solid(4).PoI = [0 -2.1066487060200863e-11 0];  % lbm*mm^2
smiData.Solid(4).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(4).opacity = 1;
smiData.Solid(4).ID = 'ZUGBÜGEL.ipt_{D387CC95-4C6F-E362-BCE4-32A319CDEE4C}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(5).mass = 0.22950213213509876;  % lbm
smiData.Solid(5).CoM = [-7.0993245005886217e-12 0 26.17681628028479];  % mm
smiData.Solid(5).MoI = [567.63931714868636 996.86692078410863 434.42261625811091];  % lbm*mm^2
smiData.Solid(5).PoI = [0 -2.3386593162483431e-11 0];  % lbm*mm^2
smiData.Solid(5).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(5).opacity = 1;
smiData.Solid(5).ID = 'LAGERBOCK.ipt_{E3292C5E-44E7-E912-7C09-B990EF3242CD}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(6).mass = 0.015439079388593518;  % lbm
smiData.Solid(6).CoM = [-6.3371844985735615e-11 -7.9999999999794795 -0.21169602778543364];  % mm
smiData.Solid(6).MoI = [2.8310299121964055 0.52070779548632851 2.8539529808183199];  % lbm*mm^2
smiData.Solid(6).PoI = [0 2.5100433529087366e-12 0];  % lbm*mm^2
smiData.Solid(6).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(6).opacity = 1;
smiData.Solid(6).ID = 'EINSATZ.ipt_{33780916-475A-D3B8-7BCA-91B12077239C}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(7).mass = 7.7835923852723523;  % lbm
smiData.Solid(7).CoM = [-1.9999999999999867 -0.8231089859612174 -0.35547332140914273];  % mm
smiData.Solid(7).MoI = [29497.840642308791 27339.378554982482 36423.21588592208];  % lbm*mm^2
smiData.Solid(7).PoI = [-295.23325143179341 -3.5715711830272376e-13 -1.3675358082380606e-12];  % lbm*mm^2
smiData.Solid(7).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(7).opacity = 1;
smiData.Solid(7).ID = 'AKKU.ipt_{965BD79C-4FE0-863F-AB31-918BF1E5723D}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(8).mass = 0.22783071675234109;  % lbm
smiData.Solid(8).CoM = [-3.4408973724436693 0.3565917810173827 -0.54848544614082062];  % mm
smiData.Solid(8).MoI = [722.95294174054777 328.44677563350149 876.01408242703974];  % lbm*mm^2
smiData.Solid(8).PoI = [1.1075402237352283 4.1616142217063459 -2.7268340513699614];  % lbm*mm^2
smiData.Solid(8).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(8).opacity = 1;
smiData.Solid(8).ID = 'RECHTECKHOHLPROFIL.ipt_{9DFBE69E-4BB4-0BA2-76BA-E8A3F5D11482}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(9).mass = 0.10692158621582684;  % lbm
smiData.Solid(9).CoM = [-1.2433293294854935 0.29691446674333338 -2.4469702352006766];  % mm
smiData.Solid(9).MoI = [167.65915756464796 62.213356381651515 228.97663062211532];  % lbm*mm^2
smiData.Solid(9).PoI = [-0.14117603298064588 0.59117463810588866 -1.1029813890930278];  % lbm*mm^2
smiData.Solid(9).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(9).opacity = 1;
smiData.Solid(9).ID = 'DECKEL.ipt_{43A8EE56-4D74-5EEE-83BB-1B941A1CD7DD}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(10).mass = 0.10738641724029008;  % lbm
smiData.Solid(10).CoM = [1.1436448345835111e-12 2.6150463522869623 -2.2534001493600626];  % mm
smiData.Solid(10).MoI = [155.56722072421806 67.009128730461043 221.50833225255505];  % lbm*mm^2
smiData.Solid(10).PoI = [0.25463481399984256 -2.861432735180716e-13 1.7077692247697364e-13];  % lbm*mm^2
smiData.Solid(10).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(10).opacity = 1;
smiData.Solid(10).ID = 'BODEN.ipt_{0603ADE9-4985-10EF-38E0-1DB0D8198E89}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(11).mass = 0.050100875527290235;  % lbm
smiData.Solid(11).CoM = [2.1274413862936923 0 -9.0000000000291962];  % mm
smiData.Solid(11).MoI = [4.9024723799848884 9.1163525721409364 11.430145824963498];  % lbm*mm^2
smiData.Solid(11).PoI = [0 3.4675570047145145e-11 0];  % lbm*mm^2
smiData.Solid(11).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(11).opacity = 1;
smiData.Solid(11).ID = 'SICHERUNG.ipt_{7D5F30E3-434B-EDD1-120F-16BE3AF7F629}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(12).mass = 0.022614021690596814;  % lbm
smiData.Solid(12).CoM = [0.2300471520397325 5.9146909388673538e-10 -1.0792562322946797e-09];  % mm
smiData.Solid(12).MoI = [2.0931053278147278 2.3519927806087644 2.5230317687313293];  % lbm*mm^2
smiData.Solid(12).PoI = [0.02898395146646475 -4.8522020325308499e-11 -5.041746606354522e-11];  % lbm*mm^2
smiData.Solid(12).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(12).opacity = 1;
smiData.Solid(12).ID = 'STECKER.ipt_{1CF58A05-43EF-6C5D-C120-D982FEA16F7C}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(13).mass = 0.023376736236236782;  % lbm
smiData.Solid(13).CoM = [1.3150741831271545e-12 9.628068531839333e-10 -1.9727336248889027];  % mm
smiData.Solid(13).MoI = [2.467329285373733 2.467329141957979 1.3837659669203612];  % lbm*mm^2
smiData.Solid(13).PoI = [1.6202268696857079e-11 0 0];  % lbm*mm^2
smiData.Solid(13).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(13).opacity = 1;
smiData.Solid(13).ID = 'STECKER_B.ipt_{B0CB72C0-48D0-A328-C574-9CA9824374CD}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(14).mass = 0.0093273334061931227;  % lbm
smiData.Solid(14).CoM = [-8.3972477493242075e-15 1.2096903098752183 3.0918666213011733e-11];  % mm
smiData.Solid(14).MoI = [0.31816758940185147 0.98990086391684617 1.0603038840924992];  % lbm*mm^2
smiData.Solid(14).PoI = [-5.5460550039321212e-13 0 0];  % lbm*mm^2
smiData.Solid(14).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(14).opacity = 1;
smiData.Solid(14).ID = 'POM_KLOTZ.ipt_{AF34FFBD-4CB6-3798-2FC6-9BAA2B9AE71F}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(15).mass = 0.0025621687282888143;  % lbm
smiData.Solid(15).CoM = [10.254003761860462 0 -2.3367717677501213e-10];  % mm
smiData.Solid(15).MoI = [0.017933330965397815 0.29804119145559499 0.29804400430312172];  % lbm*mm^2
smiData.Solid(15).PoI = [0 2.5351188921646112e-12 0];  % lbm*mm^2
smiData.Solid(15).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(15).opacity = 1;
smiData.Solid(15).ID = 'SECHSKANTSCHRAUBE - DIN 933 - M6  X 30.ipt_{054E19E2-4C8A-2F4C-22CD-04B60D752608}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(16).mass = 0.00074159889335473968;  % lbm
smiData.Solid(16).CoM = [2.6000000000001275 1.8210327570003907e-12 6.1716590638178981e-10];  % mm
smiData.Solid(16).MoI = [0.01238064334008275 0.0077363328807681401 0.0077363336043610913];  % lbm*mm^2
smiData.Solid(16).PoI = [0 0 0];  % lbm*mm^2
smiData.Solid(16).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(16).opacity = 1;
smiData.Solid(16).ID = 'SECHSKANTMUTTER - DIN 934 - M6.ipt_{12A5DB92-4E65-15A3-93FF-CEBBAFAA6724}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(17).mass = 0.24169033982622509;  % lbm
smiData.Solid(17).CoM = [-1.260255538417274 1.0642390455118316 -0.0949506224893997];  % mm
smiData.Solid(17).MoI = [737.54200086892683 354.57163251687706 909.54068856533502];  % lbm*mm^2
smiData.Solid(17).PoI = [-1.2077547168652185 0.8835162071119651 -7.9758431161055547];  % lbm*mm^2
smiData.Solid(17).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(17).opacity = 1;
smiData.Solid(17).ID = 'RECHTECKHOHLPROFIL_1.ipt_{80B1A6E8-4CB4-0675-6A83-5B92B12C5FD0}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(18).mass = 0.10956713336204536;  % lbm
smiData.Solid(18).CoM = [-0.40443619668777958 0.48290889157003353 -2.3395961618443937];  % mm
smiData.Solid(18).MoI = [168.63678296183187 65.394188671603587 233.01710719789619];  % lbm*mm^2
smiData.Solid(18).PoI = [-0.2296121248341754 0.19230015454807023 -1.7939156884859049];  % lbm*mm^2
smiData.Solid(18).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(18).opacity = 1;
smiData.Solid(18).ID = 'DECKEL_1.ipt_{C1D905F7-4F29-5892-6D31-4C8B348815F8}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(19).mass = 0.11176159812740379;  % lbm
smiData.Solid(19).CoM = [1.0988740626389999e-12 0 -2.28854546089112];  % mm
smiData.Solid(19).MoI = [174.37636431149474 67.916227485157876 241.20721267285523];  % lbm*mm^2
smiData.Solid(19).PoI = [0 -2.9045953675474469e-13 0];  % lbm*mm^2
smiData.Solid(19).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(19).opacity = 1;
smiData.Solid(19).ID = 'BODEN_1.ipt_{4823B562-4136-DD2A-A454-50B5423B87AA}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(20).mass = 0.050100875527290235;  % lbm
smiData.Solid(20).CoM = [2.1274413862936923 0 -9.0000000000291962];  % mm
smiData.Solid(20).MoI = [4.9024723799848884 9.1163525721409364 11.430145824963498];  % lbm*mm^2
smiData.Solid(20).PoI = [0 3.4675570047145145e-11 0];  % lbm*mm^2
smiData.Solid(20).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(20).opacity = 1;
smiData.Solid(20).ID = 'SICHERUNG_1.ipt_{A44D8BAC-4AAB-A8C2-F034-92800EF31B85}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(21).mass = 0.022614021690596814;  % lbm
smiData.Solid(21).CoM = [0.2300471520397325 5.9146909388673538e-10 -1.0792562322946797e-09];  % mm
smiData.Solid(21).MoI = [2.0931053278147278 2.3519927806087644 2.5230317687313293];  % lbm*mm^2
smiData.Solid(21).PoI = [0.02898395146646475 -4.8522020325308499e-11 -5.041746606354522e-11];  % lbm*mm^2
smiData.Solid(21).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(21).opacity = 1;
smiData.Solid(21).ID = 'STECKER_1.ipt_{167C9299-4C92-4E35-A905-3DAE988FA20F}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(22).mass = 0.0038452593677119297;  % lbm
smiData.Solid(22).CoM = [5.5583083051044149 7.3870193116642202e-10 -2.4573875674581168e-10];  % mm
smiData.Solid(22).MoI = [0.080745522498769123 0.35415798558655731 0.35415799524147018];  % lbm*mm^2
smiData.Solid(22).PoI = [0 9.1745701470536867e-12 -9.9343756558749168e-12];  % lbm*mm^2
smiData.Solid(22).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(22).opacity = 1;
smiData.Solid(22).ID = 'BUCHSE_BANANE.ipt_{595EBD68-403C-87E7-8B5F-7B8D885E995D}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(23).mass = 0.0038452593677119297;  % lbm
smiData.Solid(23).CoM = [5.5583083051044149 7.3870193116642202e-10 -2.4573875674581168e-10];  % mm
smiData.Solid(23).MoI = [0.080745522498769123 0.35415798558655731 0.35415799524147018];  % lbm*mm^2
smiData.Solid(23).PoI = [0 9.1745701470536867e-12 -9.9343756558749168e-12];  % lbm*mm^2
smiData.Solid(23).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(23).opacity = 1;
smiData.Solid(23).ID = 'BUCHSE_BANANE_B.ipt_{9108CA06-4170-8888-9C99-E5884C593AE3}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(24).mass = 13.206201630556571;  % lbm
smiData.Solid(24).CoM = [0.82429315289611815 0.49999999999975403 -6.0731848568503179e-15];  % mm
smiData.Solid(24).MoI = [357154.39548186469 357154.39548170392 713595.65607551858];  % lbm*mm^2
smiData.Solid(24).PoI = [-6.4563981526162353e-12 -6.4824076645822752e-12 -219.77257825492217];  % lbm*mm^2
smiData.Solid(24).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(24).opacity = 1;
smiData.Solid(24).ID = 'AKKUPLATTFORM_BUCHENMULTIPLEX.ipt_{D405E04B-41AC-FD6F-7E49-20B47CFBABA4}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(25).mass = 3.8812672361367677;  % lbm
smiData.Solid(25).CoM = [2.4573671238320935 0.42637167009757637 0];  % mm
smiData.Solid(25).MoI = [107394.05154294304 113176.31832152116 220554.19791764696];  % lbm*mm^2
smiData.Solid(25).PoI = [4.0101851879604069e-13 -4.0101851879604069e-13 23.864982001654422];  % lbm*mm^2
smiData.Solid(25).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(25).opacity = 1;
smiData.Solid(25).ID = 'PLATTFORM_ALU.ipt_{5DF80569-41B7-0539-1FE0-2EA91BFBDE52}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(26).mass = 13.972562050092376;  % lbm
smiData.Solid(26).CoM = [2.4573671238317103 0.42637167009722232 -8.610128565363345e-15];  % mm
smiData.Solid(26).MoI = [386966.73522567732 407782.89562855969 793995.11250353209];  % lbm*mm^2
smiData.Solid(26).PoI = [-5.1381665286572903e-11 -5.8042301623870977e-11 85.913935203662419];  % lbm*mm^2
smiData.Solid(26).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(26).opacity = 1;
smiData.Solid(26).ID = 'PLATTFORM_BUCHENMULTIPLESX.ipt_{B42826BC-427D-C8FE-37E3-9496BF5E395B}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(27).mass = 3.8847900582585946;  % lbm
smiData.Solid(27).CoM = [1.7355093929686549 4.1291139318432278e-14 0];  % mm
smiData.Solid(27).MoI = [107620.6614672789 113288.25037169263 220892.72521372879];  % lbm*mm^2
smiData.Solid(27).PoI = [4.0101851879604069e-13 0 1.0293912937423636e-10];  % lbm*mm^2
smiData.Solid(27).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(27).opacity = 1;
smiData.Solid(27).ID = 'PLATTFORM_ALU-SCHICHT.ipt_{22ED8E68-4D59-145E-CE9B-3BABFD955FA3}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(28).mass = 0.14853936290444275;  % lbm
smiData.Solid(28).CoM = [3.3349728327088628 7.5095944701100779 17.567028363546527];  % mm
smiData.Solid(28).MoI = [84.436792887055176 259.19604957042964 207.29896493927575];  % lbm*mm^2
smiData.Solid(28).PoI = [11.767314169868559 8.7022621058214291 9.481856240580969];  % lbm*mm^2
smiData.Solid(28).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(28).opacity = 1;
smiData.Solid(28).ID = 'HALTER_LINKS.ipt_{F8DA737B-4065-4FD5-7CB1-F89398D227F3}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(29).mass = 0.02269512374989268;  % lbm
smiData.Solid(29).CoM = [7.2757881873560484 9.8384172290273746 0];  % mm
smiData.Solid(29).MoI = [4.160554383892241 39.242191320364199 43.387615621756503];  % lbm*mm^2
smiData.Solid(29).PoI = [0 0 4.0405401428586671];  % lbm*mm^2
smiData.Solid(29).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(29).opacity = 1;
smiData.Solid(29).ID = 'GUMMIAUFLAGE.ipt_{4E5AD57F-4002-09C9-7A64-32A98ACA66C5}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(30).mass = 0.14853936290444247;  % lbm
smiData.Solid(30).CoM = [3.2898468540415635 7.5095944701100992 17.567028363546569];  % mm
smiData.Solid(30).MoI = [84.436792887054381 259.19604957047591 207.29896493932176];  % lbm*mm^2
smiData.Solid(30).PoI = [11.767314169868653 -8.7022621059555227 -9.4818562405473887];  % lbm*mm^2
smiData.Solid(30).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(30).opacity = 1;
smiData.Solid(30).ID = 'HALTER_RECHTS.ipt_{E706F00B-414F-0F45-8894-37B958B6DDAC}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(31).mass = 0.022695123749892677;  % lbm
smiData.Solid(31).CoM = [7.2757881873560493 9.8384172290273746 0];  % mm
smiData.Solid(31).MoI = [4.1605543838922472 39.242191320364192 43.38761562175651];  % lbm*mm^2
smiData.Solid(31).PoI = [0 0 4.0405401428586796];  % lbm*mm^2
smiData.Solid(31).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(31).opacity = 1;
smiData.Solid(31).ID = 'GUMMIAUFLAGE_1.ipt_{712FDDB6-47F5-57D1-7ECB-6EA6010D8837}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(32).mass = 0.15687063570851084;  % lbm
smiData.Solid(32).CoM = [6.6248196867237574 6.4981728246958212 16.634057670895956];  % mm
smiData.Solid(32).MoI = [90.757282068203793 291.08050884518462 240.58494726174513];  % lbm*mm^2
smiData.Solid(32).PoI = [9.1281153818888594 -7.000279518733385e-11 2.3703170770165196e-11];  % lbm*mm^2
smiData.Solid(32).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(32).opacity = 1;
smiData.Solid(32).ID = 'HALTER_MITTE.ipt_{9C4CB3E4-4590-BC8D-3762-188C6617B7C3}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(33).mass = 0.025472214684582089;  % lbm
smiData.Solid(33).CoM = [-3.8989441544592325e-12 7.1811658523282542 0];  % mm
smiData.Solid(33).MoI = [5.9646296793697564 49.702169213746167 55.649817416659531];  % lbm*mm^2
smiData.Solid(33).PoI = [0 0 -2.5177789723832881e-12];  % lbm*mm^2
smiData.Solid(33).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(33).opacity = 1;
smiData.Solid(33).ID = 'GUMMIAUFLAGE_2.ipt_{FB540E10-45AB-D11C-8916-3DB9A988BF46}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(34).mass = 13.985244209730954;  % lbm
smiData.Solid(34).CoM = [1.7355093929682588 -1.8351617474858773e-13 -7.168600576116708e-15];  % mm
smiData.Solid(34).MoI = [387782.84695043048 408186.16700632102 795213.81076942605];  % lbm*mm^2
smiData.Solid(34).PoI = [-4.491407410515654e-11 -3.2255474355214443e-11 -4.1509718024634405e-10];  % lbm*mm^2
smiData.Solid(34).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(34).opacity = 1;
smiData.Solid(34).ID = 'PLATTFORM_BUCHEN-MULTIPLEX-SCHICHT.ipt_{E05F58A9-41A8-D24B-B24D-9FA30CE75B0D}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(35).mass = 7.5811981957509404;  % lbm
smiData.Solid(35).CoM = [-0.00058802722553747799 1.7462600832415969e-05 45.50000000000005];  % mm
smiData.Solid(35).MoI = [1142375.9076876461 1138767.510064991 8955.9723104338445];  % lbm*mm^2
smiData.Solid(35).PoI = [-4.2269356331195688e-10 7.4930104636725393e-11 0.035998206759220851];  % lbm*mm^2
smiData.Solid(35).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(35).opacity = 1;
smiData.Solid(35).ID = 'STRINGER_1.ipt_{AC78C309-4331-CFB0-3D7F-79AB05B16283}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(36).mass = 0.0064444094643314499;  % lbm
smiData.Solid(36).CoM = [-0.92714086070226476 2.0000010709691169 1.611286536482059];  % mm
smiData.Solid(36).MoI = [0.2768234030410891 0.29258724609598102 0.29612631198646266];  % lbm*mm^2
smiData.Solid(36).PoI = [5.0630454111512268e-08 -0.0035509533449683546 -4.0416962630567628e-08];  % lbm*mm^2
smiData.Solid(36).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(36).opacity = 1;
smiData.Solid(36).ID = 'SENSORKLOTZ.ipt_{5FA18C17-48C1-571D-0A80-9F8C8ADB962B}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(37).mass = 1.4261605886549922;  % lbm
smiData.Solid(37).CoM = [3.3890122446501824e-11 0 9.7354139340718457e-11];  % mm
smiData.Solid(37).MoI = [112370.51235016872 224281.27420125788 112370.51235016603];  % lbm*mm^2
smiData.Solid(37).PoI = [-1.641982857039101e-11 1.633649190950076e-09 7.6318836858371498e-12];  % lbm*mm^2
smiData.Solid(37).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(37).opacity = 1;
smiData.Solid(37).ID = 'SPANNKLOTZ.ipt_{31EA13D2-43D0-0E30-3E30-CE914430C39B}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(38).mass = 0.16851021824252452;  % lbm
smiData.Solid(38).CoM = [-2.1537077293917563e-11 -3.5696813194338503e-13 -1.8443353483741558e-12];  % mm
smiData.Solid(38).MoI = [13866.747986730052 27708.219440725865 13866.747986732189];  % lbm*mm^2
smiData.Solid(38).PoI = [6.2157870413386413e-11 -3.8818592619456071e-10 -6.2959907450977086e-11];  % lbm*mm^2
smiData.Solid(38).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(38).opacity = 1;
smiData.Solid(38).ID = 'SPANNBAND.ipt_{8E255633-414F-E7E3-9A0E-139AD8F1EDF2}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(39).mass = 7.8844532288891411;  % lbm
smiData.Solid(39).CoM = [1.150751196610084e-10 38.044260727684879 8.8617823319972738e-12];  % mm
smiData.Solid(39).MoI = [283616.56413900817 305269.65665973496 283616.56411710067];  % lbm*mm^2
smiData.Solid(39).PoI = [1.9889406860579159e-08 -3.775353593544322e-10 2.7567101342144776e-07];  % lbm*mm^2
smiData.Solid(39).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(39).opacity = 1;
smiData.Solid(39).ID = 'KEGEL_05.ipt_{D8540125-4F0B-5656-6480-03A795C168EF}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(40).mass = 0.15245369652133448;  % lbm
smiData.Solid(40).CoM = [1.3530043660377592e-08 -5.9013307511181896 -0.00010245341149637296];  % mm
smiData.Solid(40).MoI = [63.768405289516259 111.37895441577669 63.768376791465343];  % lbm*mm^2
smiData.Solid(40).PoI = [-0.00011563145571211757 -6.7178443475402586e-05 6.3037465931191684e-09];  % lbm*mm^2
smiData.Solid(40).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(40).opacity = 1;
smiData.Solid(40).ID = 'KONTAKTPLATTE.ipt_{A39D6713-48FE-BD4D-CE21-E79A10790FDB}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(41).mass = 0.24712742094952231;  % lbm
smiData.Solid(41).CoM = [-13.172309894710972 14.617638457686919 12.607122755138898];  % mm
smiData.Solid(41).MoI = [320.2223126574865 101.41127464030649 264.25618572288653];  % lbm*mm^2
smiData.Solid(41).PoI = [0.22113214283953367 1.2747168977285046 0.1248798737628813];  % lbm*mm^2
smiData.Solid(41).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(41).opacity = 1;
smiData.Solid(41).ID = 'FUSS_KATAPULT_KAPSEL_SENSOR.ipt_{D17DBD7A-48B5-0F65-593D-0CB2FCA5CBDA}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(42).mass = 0.24734711934552636;  % lbm
smiData.Solid(42).CoM = [-13.19134080674228 14.65211666199381 12.633314537120288];  % mm
smiData.Solid(42).MoI = [320.35555197455199 99.58538304231503 262.2034627081004];  % lbm*mm^2
smiData.Solid(42).PoI = [-0.00088153087610812024 1.270280714712378 0.064922930400320239];  % lbm*mm^2
smiData.Solid(42).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(42).opacity = 1;
smiData.Solid(42).ID = 'FUSS_KATAPULT_KAPSEL_REV_2.ipt_{DC1D7B52-4A6A-6965-EF16-CC8C998579C8}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(43).mass = 0.048961345904539877;  % lbm
smiData.Solid(43).CoM = [1.9514919998508884e-09 3.4999999999999947 -1.3925471068790646e-10];  % mm
smiData.Solid(43).MoI = [16.564809243367854 5.0154938854413231 16.564808888346629];  % lbm*mm^2
smiData.Solid(43).PoI = [1.4314872966359097e-11 0 -2.8645880662185784e-11];  % lbm*mm^2
smiData.Solid(43).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(43).opacity = 1;
smiData.Solid(43).ID = 'GEWINDESTUECK_OBEN.ipt_{BB89C2F5-4763-1150-E7D7-27BCE63DDE18}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(44).mass = 0.31568415175807557;  % lbm
smiData.Solid(44).CoM = [-17.281296849482498 -69.746887974112553 5.8620852762097906e-05];  % mm
smiData.Solid(44).MoI = [3597.8909244242868 51.533108930415565 3597.379301594066];  % lbm*mm^2
smiData.Solid(44).PoI = [0.010452501980076757 -0.00088878457652979135 0.0033247413684514781];  % lbm*mm^2
smiData.Solid(44).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(44).opacity = 1;
smiData.Solid(44).ID = 'MITTELSTUECK.ipt_{0AF8DABF-46B3-1D20-9981-31A52886B3DC}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(45).mass = 0.06177509482405126;  % lbm
smiData.Solid(45).CoM = [-1.158930907509603e-10 3.958471658372102 -9.9843998550957821e-11];  % mm
smiData.Solid(45).MoI = [48.193698074057153 4.830699620745408 48.193697739654091];  % lbm*mm^2
smiData.Solid(45).PoI = [4.5384323940139509e-11 0 2.9461548622637381e-12];  % lbm*mm^2
smiData.Solid(45).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(45).opacity = 1;
smiData.Solid(45).ID = 'ZENTRIERSTUECK_UNTEN.ipt_{7227D8B7-4565-8B37-3833-128D2F9BB7FA}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(46).mass = 17.909925343679745;  % lbm
smiData.Solid(46).CoM = [-7.5451386689793311e-06 15.0179027581433 -2.6829596617046914e-05];  % mm
smiData.Solid(46).MoI = [743455.67044028174 1471472.1616421575 743455.68866302667];  % lbm*mm^2
smiData.Solid(46).PoI = [0.00089841580404443927 -0.035151134421689656 0.00025427298868159281];  % lbm*mm^2
smiData.Solid(46).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(46).opacity = 1;
smiData.Solid(46).ID = 'FALLKAPSEL_BODEN_080610.ipt_{3BA3A6C8-40DA-DE96-3DF0-0F99DDEE5F08}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(47).mass = 13.931007364342108;  % lbm
smiData.Solid(47).CoM = [-0.38783333640150774 8.2593415325898523 0.15343139167961933];  % mm
smiData.Solid(47).MoI = [633540.63978315529 1257577.0973405731 633346.25876775128];  % lbm*mm^2
smiData.Solid(47).PoI = [360.6992359928654 6777.5976876316099 514.78107888000011];  % lbm*mm^2
smiData.Solid(47).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(47).opacity = 1;
smiData.Solid(47).ID = 'KAPSEL_DECKEL.ipt_{42FD36A1-4E50-B2B4-BF41-308D59887FDF}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(48).mass = 0.01338618147047429;  % lbm
smiData.Solid(48).CoM = [-3.3531385288402524e-09 7.9376701028006647 -5.8511032160373491e-15];  % mm
smiData.Solid(48).MoI = [1.5287152635862755 2.6518768666423131 1.528714993968215];  % lbm*mm^2
smiData.Solid(48).PoI = [0 0 4.4119939390244338e-11];  % lbm*mm^2
smiData.Solid(48).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(48).opacity = 1;
smiData.Solid(48).ID = 'SICHERUNGSHUELSE.ipt_{3B0E81C0-4375-0C7E-A2F6-65BA6807C249}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(49).mass = 0.28696325008841789;  % lbm
smiData.Solid(49).CoM = [1.6420039573578058 0 -16.181187283148805];  % mm
smiData.Solid(49).MoI = [586.09764660402209 2922.0255232247177 2336.358321495828];  % lbm*mm^2
smiData.Solid(49).PoI = [0 -273.69770916111491 0];  % lbm*mm^2
smiData.Solid(49).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(49).opacity = 1;
smiData.Solid(49).ID = 'ABDECKBLECH.ipt_{37CE83A5-4B26-06ED-1B5F-5C875AFF923E}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(50).mass = 0.03992418364931731;  % lbm
smiData.Solid(50).CoM = [-9.0927456613814642e-10 1.1770900085070268e-14 8.0113968921816507];  % mm
smiData.Solid(50).MoI = [11.796791746623262 11.796791963885568 2.0006456570238895];  % lbm*mm^2
smiData.Solid(50).PoI = [0 -4.1255586855243264e-11 0];  % lbm*mm^2
smiData.Solid(50).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(50).opacity = 1;
smiData.Solid(50).ID = 'BLITZSCHUTZ.ipt_{B546B178-4591-3C80-D68D-7F9BDA4A3133}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(51).mass = 0.05071224929053688;  % lbm
smiData.Solid(51).CoM = [1.6599936363617411e-09 0 0.029531820718746285];  % mm
smiData.Solid(51).MoI = [21.297956314903807 21.297956057506759 2.6142370324917352];  % lbm*mm^2
smiData.Solid(51).PoI = [0 4.0833558682444319e-10 0];  % lbm*mm^2
smiData.Solid(51).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(51).opacity = 1;
smiData.Solid(51).ID = 'DRUCKSENSOR.ipt_{EEB93F85-48FD-BD39-4FAD-20BA602CA11C}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(52).mass = 0.013708213058323393;  % lbm
smiData.Solid(52).CoM = [1.5820525063179998e-10 -0.24017048682050943 -4.2442499843801245e-12];  % mm
smiData.Solid(52).MoI = [1.2151380293331457 1.2150443652844627 2.2805966491510632];  % lbm*mm^2
smiData.Solid(52).PoI = [5.4047491619423852e-13 0 -5.2105625434028519e-13];  % lbm*mm^2
smiData.Solid(52).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(52).opacity = 1;
smiData.Solid(52).ID = 'HALTER_1.ipt_{60A3ED79-4176-EFCF-53E9-A987F17F6976}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(53).mass = 0.020776253643085725;  % lbm
smiData.Solid(53).CoM = [5.1695763559765728e-10 12.267402857300759 -9.8273751528454769e-05];  % mm
smiData.Solid(53).MoI = [4.701820237970451 1.9363279933781214 6.4125781480650623];  % lbm*mm^2
smiData.Solid(53).PoI = [-1.142579499156576e-05 0 1.3175185797997498e-10];  % lbm*mm^2
smiData.Solid(53).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(53).opacity = 1;
smiData.Solid(53).ID = 'HALTER_2.ipt_{75517159-47A4-03B3-9F59-14A81211715D}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(54).mass = 0.55943903032423925;  % lbm
smiData.Solid(54).CoM = [13.075569189694745 -4.1137065785858082 -0.046867989184613601];  % mm
smiData.Solid(54).MoI = [1252.3264293544314 1739.8765530684323 2983.109553660905];  % lbm*mm^2
smiData.Solid(54).PoI = [2.5894258220366778e-12 -0.53465904318655211 -6.7268976753728964e-10];  % lbm*mm^2
smiData.Solid(54).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(54).opacity = 1;
smiData.Solid(54).ID = 'DECKPLATTE_ANDOCKSYSTEM.ipt_{04923966-4848-4DEB-B479-1CA5408FCD88}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(55).mass = 0.05100557178700954;  % lbm
smiData.Solid(55).CoM = [-1.4950318067521517 -1.2284764461328874e-14 0.18883453535785272];  % mm
smiData.Solid(55).MoI = [22.929099897552561 5.5253107539651198 25.115466442245555];  % lbm*mm^2
smiData.Solid(55).PoI = [0 -0.041905774160098073 0];  % lbm*mm^2
smiData.Solid(55).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(55).opacity = 1;
smiData.Solid(55).ID = 'AUSRICHTHILFE.ipt_{172D4160-4679-DB27-CA06-ED86741C4204}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(56).mass = 1.3426002466856706;  % lbm
smiData.Solid(56).CoM = [3.7758158353378587e-05 3.0680274776717402 79.909135314562121];  % mm
smiData.Solid(56).MoI = [9013.5026166238422 10248.639303822385 2858.069225342183];  % lbm*mm^2
smiData.Solid(56).PoI = [142.36118440971345 -0.0053987025800269189 0.00023430157882160205];  % lbm*mm^2
smiData.Solid(56).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(56).opacity = 1;
smiData.Solid(56).ID = 'STRINGERFUSS_VAR_2_REV_2.ipt_{CCC76D9B-497F-3190-2543-24B9B9EDF4E6}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(57).mass = 0.16701433068067503;  % lbm
smiData.Solid(57).CoM = [3.1615533575896328e-08 -6.9707005538684831e-12 2.2564469966847183];  % mm
smiData.Solid(57).MoI = [69.59561951641173 69.595619277464536 124.68103245686812];  % lbm*mm^2
smiData.Solid(57).PoI = [-1.7677697418111617e-11 7.0034800814523195e-08 -2.9183019904491973e-08];  % lbm*mm^2
smiData.Solid(57).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(57).opacity = 1;
smiData.Solid(57).ID = 'MITTELTEIL.ipt_{A998209D-4431-C8EB-3BE2-CEAC47154D94}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(58).mass = 0.036718232766324715;  % lbm
smiData.Solid(58).CoM = [3.5314217136923576 -3.4502103051586075 3.0928101533401966];  % mm
smiData.Solid(58).MoI = [3.529352304335494 6.474304291247849 6.9386489708098535];  % lbm*mm^2
smiData.Solid(58).PoI = [0.00034064324463335536 0.42456069565678251 9.8243138157577502e-05];  % lbm*mm^2
smiData.Solid(58).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(58).opacity = 1;
smiData.Solid(58).ID = 'VERBINDUNGSSTUECK.ipt_{2C00B493-42EC-FD9D-58D5-E49A897985E4}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(59).mass = 0.13378288692489063;  % lbm
smiData.Solid(59).CoM = [3.5244469059731969e-09 0 -10.365000000000073];  % mm
smiData.Solid(59).MoI = [868.48409691141478 868.48409438830402 19.536677881805822];  % lbm*mm^2
smiData.Solid(59).PoI = [0 6.7773236948187425e-08 0];  % lbm*mm^2
smiData.Solid(59).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(59).opacity = 1;
smiData.Solid(59).ID = 'ROHR.ipt_{3005A22F-4939-2EF1-9E1B-70955CE2D077}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(60).mass = 0.023129455017071181;  % lbm
smiData.Solid(60).CoM = [-1.5369850224153796e-09 5.0794925384888931e-15 0.091545238246973637];  % mm
smiData.Solid(60).MoI = [2.0461941261512195 2.0461941698413972 2.3717241379521701];  % lbm*mm^2
smiData.Solid(60).PoI = [0 6.7844147984299271e-11 0];  % lbm*mm^2
smiData.Solid(60).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(60).opacity = 1;
smiData.Solid(60).ID = 'GEWINDESTUECK.ipt_{4EC743C7-4F24-89A9-64EB-1FBAE0B7E8FA}';

