# %%
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
# %%
gyro_temp_and_acc_data = pd.read_csv('15-02-13_10-21-38_15_IMU.txt', delimiter='\t')

# %%
plt.hist(gyro_temp_and_acc_data['Temperature'])

# %%
plt.plot(gyro_temp_and_acc_data['Accel_Z']/9.81)

# %%
time_offset = 4 + 7
gyro_temp_and_acc_data['Sample Time'] = (gyro_temp_and_acc_data['Sample Time'] - gyro_temp_and_acc_data['Sample Time'].loc[0])*0.000001 + time_offset
# %%
gyro_temp_and_acc_data

# %%
acc_data = gyro_temp_and_acc_data[['Sample Time', 'Accel_X', 'Accel_Y', 'Accel_Z']]

# %%
acc_data = acc_data.set_index('Sample Time')

#%%
# add just normal gravity at the begginning
acc_data.loc[0.000000] = [0, 0, 9.81]
acc_data.loc[time_offset - 0.002500] = [0, 0, 9.81]
acc_data = acc_data.sort_index()

# %%
plt.plot(acc_data['Accel_Z']/9.81)
plt.plot(acc_data['Accel_X']/9.81)
plt.plot(acc_data['Accel_Y']/9.81)

#%%
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.animation import FuncAnimation
plt.style.use('seaborn-pastel')


fig = plt.figure()
ax = plt.axes(xlim=(0, 4), ylim=(-10, 40))
line, = ax.plot([], [], lw=3)

def init():
    line.set_data([], [])
    return line,
def animate(i):
    x = acc_data.index.to_numpy()
    y = acc_data['Accel_Z'].to_numpy()
    line.set_data(x, y)
    return line,

anim = FuncAnimation(fig, animate, init_func=init,
                               frames=200, interval=20, blit=True)


anim.save('sine_wave.gif', writer='imagemagick')
# %%
# import scipy.io as sio

# # %%
# sio.savemat('acc_data.mat', {'Z':acc_data['Accel_Z'].to_list(),
#                             'Y':acc_data['Accel_Y'].to_list(),
#                             'X':acc_data['Accel_X'].to_list(),
#                             'time_acc': acc_data.index.to_list()})

# # %%

# # %%
