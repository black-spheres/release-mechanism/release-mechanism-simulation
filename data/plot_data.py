#%%
import numpy as np
import matplotlib.pyplot as plt
# %%
with open('bs.log', 'r') as f:
    velocity_string = f.read()
# %%
velocity_string_vector = velocity_string.split('\n')
# %%
import math
velocity_vector = [float(x)*2*math.pi for x in velocity_string_vector[0:200]] #rad/s
# %%
time_vector = np.linspace(0, 10, 200)
plt.plot(time_vector, velocity_vector, '-')
# 48 sample, 2.4 s

# %%
condition = ((time_vector >= 2.4) & (time_vector < 4.4)) | ((time_vector >= 6.4) & (time_vector < 8.4))
# %%
control_vector = np.where(condition, 12.0, 0.0)

# %%
import scipy.io as sio
sio.savemat('polou_dc.mat', {'velocity_rads':velocity_vector,
                            'time_dc': list(time_vector),
                            'control_dc': list(control_vector)})
# %%
