clear all
tic
seed = 5;
rng(seed,'twister');
screw_pitch = 0.002; %screw pitch in m
radius = 71/2000; %radius of the sphere in m
number_of_points = 1000;


x_joint_init_max =0; %m
y_joint_init_max =0;
z_joint_init_max = 0;
x_dot_joint_init_max = 0; %m/s
y_dot_joint_init_max = 0;
z_dot_joint_init_max = 0.0015;
omega_joint_init_max  = [0,0,20*pi]; %deg/s
natural_l_max = radius*1.1; %m
mass_vel_init_max = 0; %m/s
mass_pos_init_max = 50; %m
spring_k_max = 12; %N/m
spring_d_max = 0.0001;  %N/(m/s)
payload_d_max = 19000; %kg/mmm

x_joint_init_min =0; %m
y_joint_init_min =0;
z_joint_init_min = 0;
x_dot_joint_init_min = 0; %m/s
y_dot_joint_init_min = 0;
z_dot_joint_init_min = 0.0015;
omega_joint_init_min  = [0,0,.01]; %rad/s
natural_l_min = radius*1.1; %m
mass_vel_init_min = 0; %m/s
mass_pos_init_min = 0; %m
spring_k_min = 12; %N/m
spring_d_min = 0.0001;  %N/(m/s)
payload_d_min = 3000; %kg/mmm


omega_joint_mc  = (omega_joint_init_max-omega_joint_init_min).*rand(number_of_points,3) + omega_joint_init_min;
x_joint_mc = (x_joint_init_max-x_joint_init_min).*rand(number_of_points,1) + x_joint_init_min; 
y_joint_mc = (y_joint_init_max-y_joint_init_min).*rand(number_of_points,1) + y_joint_init_min;
z_joint_mc = (z_joint_init_max-z_joint_init_min).*rand(number_of_points,1) + z_joint_init_min;
x_dot_joint_mc = (x_dot_joint_init_max-x_dot_joint_init_min).*rand(number_of_points,1) + x_dot_joint_init_min; 
y_dot_joint_mc = (y_dot_joint_init_max-y_dot_joint_init_min).*rand(number_of_points,1) + y_dot_joint_init_min; 
z_dot_joint_mc = screw_pitch/360.*omega_joint_mc;%(z_dot_joint_init_max-z_dot_joint_init_min).*rand(number_of_points,1) + z_dot_joint_init_min; 
natural_l_mc = (natural_l_max-natural_l_min).*rand(number_of_points,1) + natural_l_min; 
mass_vel_mc = zeros(number_of_points,1);%(mass_vel_init_max-mass_vel_init_min).*rand(number_of_points,1) + mass_vel_init_min; 
spring_k_mc = (spring_k_max-spring_k_min).*rand(number_of_points,1) + spring_k_min; 
spring_d_mc = (spring_d_max-spring_d_min).*rand(number_of_points,1) + spring_d_min; 
payload_d_mc = (payload_d_max-payload_d_min).*rand(number_of_points,1) + payload_d_min; 
m = (payload_d_mc * 2/10^9 ); %kg
mass_pos_mc = (-spring_k_mc .* natural_l_mc - m .*omega_joint_mc(:,3).*omega_joint_mc(:,3)*radius)./( spring_k_mc - m.*omega_joint_mc(:,3).*omega_joint_mc(:,3) );%mass_pos_init_max-mass_pos_init_min).*rand(number_of_points,1) + mass_pos_init_min; 
final_position = zeros(number_of_points,3);

for it = 1:number_of_points
    x_joint_init = x_joint_mc(it);
    y_joint_init = y_joint_mc(it);
    z_joint_init = z_joint_mc(it);
    x_dot_joint_init = x_dot_joint_mc(it);
    y_dot_joint_init = y_dot_joint_mc(it);
    z_dot_joint_init = z_dot_joint_mc(it);
    omega_joint_init  =  omega_joint_mc(it,:);
    natural_l = natural_l_mc(it);
    mass_vel_init =  mass_vel_mc(it);
    mass_pos_init = mass_pos_mc(it);
    spring_k = spring_k_mc(it);
    spring_d = spring_d_mc(it);
    payload_d = payload_d_mc(it);
    output = sim('sphere2019b');
    final_position(it,1) = output.position(length(output.position),1);
    final_position(it,2) = output.position(length(output.position),2);
    final_position(it,3) = output.position(length(output.position),3);
end
r_position = sqrt(final_position(:,1).^2+final_position(:,2).^2);
lon =omega_joint_mc(:,3);
lat = payload_d_mc;
z =r_position;
[LON,LAT] = meshgrid(min(lon):max(lon), min(lat) :max(lat));
Z = griddata(lon,lat, z, LON, LAT);
[C,h] = contour(LON, LAT, Z);   
clabel(C,h)
xlabel('Omega_Z[rad/s]')
ylabel('Payload density kg/m*m*m')
savefig(strcat('C:\Users\Bartek\Documents\Praca Magisterska\symulacje\release-mechanism-simulation-master\MC_results\result_fig_', int2str(seed), '.fig'))
time_of_exec = toc;
save(strcat('C:\Users\Bartek\Documents\Praca Magisterska\symulacje\release-mechanism-simulation-master\MC_results\result_', int2str(seed), '.mat'), 'omega_joint_mc','x_joint_mc','y_joint_mc'...
    ,'z_joint_mc','x_dot_joint_mc','y_dot_joint_mc','z_dot_joint_mc',...
    'natural_l_mc','mass_vel_mc','spring_k_mc','spring_d_mc',...
    'payload_d_mc','m','mass_pos_mc', 'time_of_exec');
% zlabel('Max dist. from the center [m]')
% sim('sphere2019b');
% radius = 71mm
% m = payload_dens * 2 cm^3
% r = abs(mas_pos_init.-radius);
% k(natural_l - mass_pos_init) = m*omega*omega*(mas_pos_init-radius)
% k*mass_pos_init - m*omega*omega*mas_pos_init = -k * natural_l -
% m*omega*omega*radius
% mass_pos_init * ( k - m*omega*omega ) = -k * natural_l - m*omega*omega*radius
% mass_pos_init = (-k * natural_l - m*omega*omega*radius)/( k - m*omega*omega )
% mass_pos_init = natural_l - payload_d * 0.002.*omega_joint_init(:,3).*omega_joint_init(:,3).*abs(mas_pos_init.-radius)./spring_k
clear all