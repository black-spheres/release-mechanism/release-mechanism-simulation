motor_l = 0.0001;
motor_r = 1;
motor_k = 0.029;
motor_j = 0.01;
rotor_damping = 0.0001;
motor_stall_torque = 14/30; % mNm
motor_no_load_speed = 30*170*(2*pi)/60; % rad/s
rated_dc_supply = 12; % V
