GRAVITY_ZERO = Simulink.Variant('gravity==1');
GRAVITY_NORMAL = Simulink.Variant('gravity==2');
GRAVITY_ZARM = Simulink.Variant('gravity==3');
gravity = 3;

SPHERE_SPRING = Simulink.Variant('sphere_config==1');
SPHERE_EMPTY = Simulink.Variant('sphere_config==2');
SPHERE_BACKUP = Simulink.Variant('sphere_config==3');
sphere_config = 1;

MASS_SPRING = Simulink.Variant('mass_config==1');
MASS_BLOCKED = Simulink.Variant('mass_config==2');
mass_config = 1;

FRICTION_DAMPING = Simulink.Variant('friction_config==1');
FRICTION_EXTENDED = Simulink.Variant('friction_config==2');
friction_config = 1;

% FRICTION_LOAD_ENABLE = Simulink.Variant('friction_load==1');
% FRICTION_LOAD_DISABLE = Simulink.Variant('friction_load==2');
% friction_load = 2;

FRICTION_LIMITED_ENABLE = Simulink.Variant('friction_limited==1');
FRICTION_LIMITED_DISABLE = Simulink.Variant('friction_limited==2');
friction_limited = 1;

CONTROL_HARD_STOP = Simulink.Variant('control_mode==1');
CONTROL_REVERSED = Simulink.Variant('control_mode==2');
CONTROL_SPIN = Simulink.Variant('control_mode==3');
CONTROL_TEST = Simulink.Variant('control_mode==4');
control_mode = 2;

SCREW_INFINITY_ENABLED = Simulink.Variant('screw_mode==1');
SCREW_INFINITY_DISABLED = Simulink.Variant('screw_mode==2');
screw_mode = 2;