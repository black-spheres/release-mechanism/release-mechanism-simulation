I_s = 0.01; %3.0631e-07; % [kg*m^2]
I_n = 0.0114; % [kg*m^2]
lead = 0.015; % [m]
m_n = 0.3; % [kg]
init_angular_v_n = pi; %[rad/s]
init_angular_v_s = pi; %[rad/s]
A = I_s * init_angular_v_s^2 + I_n * init_angular_v_n^2;
final_angular_v = sqrt(4*pi*A/(I_n*4*pi*pi + m_n*lead));