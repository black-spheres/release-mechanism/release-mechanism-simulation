r_screw = 0.005; %[m]
screw_lead = 0.012; %[m]
lead_angle = rad2deg(atan(screw_lead/(2*pi*r_screw))); %[deg]
screw_nut_damping = 0.0000001; % Nm / (deg/s)
init_angular_velocity = 2; % Hz
sphere_initial_position = 0; % mm
camera_radius = 295; % mm
load acc_data;

%% DC motor identification results
armature_inductance = 538.92; %uH
no_load_speed = 5649.3; %rpm
rated_dc = 24.667; %V
rotor_damping = 1.2697e-05; %Nm/(rad/s)
stall_torque = 40.003; %mNm
I = 24.372;
P = 0.11147;

%% control parameters
max_angular_velocity = 2; %Hz
min_angular_velocity = -4; %Hz

%% Screw-nut friction identification results
screw_breakaway_friction_torque = 0.39545;
screw_breakaway_friction_velocity = 0.014839;
screw_coulomb_friction_torque = 0.39218;
viscous_friction_coefficient = 0.00017652/50;
%% control params
set_val = 2.5;
scale_diff = 4;

%% Odd hack
SDO_OutputTimes = 0;