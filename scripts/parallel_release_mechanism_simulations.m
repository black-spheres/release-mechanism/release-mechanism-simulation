% init_angular_velocity_vect = linspace(1,2.5,6);

% screw_viscous_friction_coefficient_vect = logspace(-10, -2, 8);
clear in
screw_viscous_friction_coefficient_vect = linspace(2e-8, 2e-4, 12);
scale_diff_vect = linspace(3.14,10,12);
% set_val_vect = linspace(4.5, 1.5, 8);
% scale_diff_vect = logspace(0,1,16);
% init_position_vect = linspace(0,12,16);
numSims = length(screw_viscous_friction_coefficient_vect);
mdl = 'black_spheres_experiments_in_zarm_model';
isModelOpen = bdIsLoaded(mdl);
open_system(mdl);
for i = numSims:-1:1
    in(i) = Simulink.SimulationInput(mdl);
    in(i) = in(i).setVariable('viscous_friction_coefficient', screw_viscous_friction_coefficient_vect(i));
    in(i) = in(i).setVariable('gravity', 3);

%    in(i) = in(i).setVariable('sphere_initial_position', init_position_vect(i));
end

out = parsim(in, 'ShowSimulationManager', 'on', 'ShowProgress', 'on');
%     'SetupFcn', @() sldemo_parsim_paramsweep_suspn_raccel_setup(mdl));