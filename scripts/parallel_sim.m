function [simout] = parallel_sim(init_angular_velocity_vect)
%UNTITLED Summary of this function goes here
%   Run simulations in parrael 
numSims = length(init_angular_velocity_vect);
mdl = 'multibodyscrewandnutfriction';
open_system(mdl);
for i = numSims:-1:1
   in(i) = Simulink.SimulationInput(mdl);
   in(i) = in(i).setBlockParameter('multibodyscrewandnutfriction/DCmotorsubsytem/init_angular_velocity', 'Gain', num2str(init_angular_velocity_vect(i)));
end

out = parsim(in, 'ShowProgress', 'on');
%     'SetupFcn', @() sldemo_parsim_paramsweep_suspn_raccel_setup(mdl));
simout = out;
end

