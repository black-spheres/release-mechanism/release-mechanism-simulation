close all;
figure()
legend_labels = cell(1,numSims);
for i = 1:numSims
        simOut = out(i);
        ts = simOut.logsout.get('com_z').Values;
        ts.plot;
        legend_labels{i} = ['viscous_friction_coefficient ' num2str(screw_viscous_friction_coefficient_vect(i))];
        hold all
end
title('Displacement(com)')
xlabel('Time (s)');
ylabel('Distance (m)');
legend(legend_labels,'Location','NorthEastOutside');

figure()
for i = 1:numSims
        simOut = out(i);
        ts = simOut.logsout.get('angular velocityx').Values;
        ts.plot;
        hold all
end
title('Sphere')
xlabel('Time (s)');
ylabel('Angular velocity x(Hz)');
legend(legend_labels,'Location','NorthEastOutside');
figure()
for i = 1:numSims
        simOut = out(i);
        ts = simOut.logsout.get('angular velocityy').Values;
        ts.plot;
        hold all
end
title('Sphere')
xlabel('Time (s)');
ylabel('Angular velocity y(Hz)');
legend(legend_labels,'Location','NorthEastOutside');

figure()
for i = 1:numSims
        simOut = out(i);
        ts = simOut.logsout.get('angular velocity').Values;
        ts.plot;
        hold all
end
title('Sphere')
xlabel('Time (s)');
ylabel('Angular velocity (Hz)');
legend(legend_labels,'Location','NorthEastOutside');

figure()
for i = 1:numSims
        simOut = out(i);
        ts = simOut.logsout.get('motor velo').Values;
        ts.plot;
        hold all
end
title('Motor Velocity')
xlabel('Time (s)');
ylabel('Angular velocity (Hz)');
legend(legend_labels,'Location','NorthEastOutside');

figure()
for i = 1:numSims
        simOut = out(i);
        x = simOut.logsout.get('com_x').Values.data;
        y = simOut.logsout.get('com_y').Values.data;
        z = simOut.logsout.get('com_z').Values.data;
        plot(x,y);
        hold all
end
title('com trajectory')
xlabel('X');
ylabel('Y');
legend(legend_labels,'Location','NorthEastOutside');

figure()
for i = 1:numSims
        simOut = out(i);
        x = simOut.logsout.get('com_x').Values.data;
        y = simOut.logsout.get('com_y').Values.data;
        z = simOut.logsout.get('com_z').Values.data;
        plot(x,z);
        hold all
end
title('com trajectory')
xlabel('X');
ylabel('Z');
legend(legend_labels,'Location','NorthEastOutside');

figure()
for i = 1:numSims
        simOut = out(i);
        x = simOut.logsout.get('com_x').Values.data;
        y = simOut.logsout.get('com_y').Values.data;
        z = simOut.logsout.get('com_z').Values.data;
        plot3(x,y,z,'LineWidth',1);
%         legend_labels{i} = ['velo diff ' num2str(scale_diff_vect(i)), 'Hz'];
        hold on
end
title('com trajectory')
xlabel('X');
ylabel('Y');
zlabel('Z');
[x_c, y_c, z_c] = cylinder(0.3, 100);
z_c = z_c*0.6;
hold on
s = surf(x_c, y_c, z_c,'FaceAlpha',0.2);
s.EdgeColor = 'none';
s.FaceColor = 'red';
legend(legend_labels,'Location','NorthEastOutside');
hold off
