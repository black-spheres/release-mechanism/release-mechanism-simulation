# %% 
import matlab.engine
eng = matlab.engine.start_matlab()
eng.openProject("C:\\Users\\Kacper\\MATLAB\\Projects\\release-mechanism-simulation\\Release_mechanism.prj")
# %%
data = eng.parallel_release_mechanism_simulations(nargout=0)
# %%
import numpy as np
import matlab
screw_viscous_friction_coefficient_vect = np.logspace(-7, -4, 12, dtype=matlab.double)
sim_data = eng.par_simulations('viscous_friction', list(screw_viscous_friction_coefficient_vect))
# %%
eng.plotter(nargout=0)
# %%
import json

data = {}
data['multibody'] = {'magic_number': 0.42, 'integral_number': 2, 'string': "XD", 'array': [1,2,3,4]}
with open('params.json', 'w') as f:
    json.dump(data, f)
# %%
