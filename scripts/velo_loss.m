velos = [];
for i = 1:numSims
        simOut = out(i);
        ts = simOut.logsout.get('angular velocity').Values;
        position = find(ts.time==10);
        first_velo = ts.Data(position);
        position = find(ts.time==14);
        second_velo = ts.Data(position);
        velo_loss_i = -(first_velo - second_velo)/scale_diff_vect(i);
        velos = [velos, velo_loss_i];
end
plot(scale_diff_vect, velos)
title('Nut trajectory')
title('Velocity loss')
xlabel('velo diff');
ylabel('loss');