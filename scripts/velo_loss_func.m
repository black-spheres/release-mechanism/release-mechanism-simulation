function [y] = velo_loss_func(diff)
% function of velocity loss based on difference between set velocities 
a = 1.374;
b = 0.563;
c = 0.103;
y = a*exp(-b*diff)+c;
end

